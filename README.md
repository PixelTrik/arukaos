# ArukaOS
A toy 32-bit x86 (i386) operating system written to learn how operating systems work.

![](multi-task.gif)


## Why did I built it?
ArukaOS is a 32-bit, simple operating system with rudimentary features like a read-only file system, basic multitasking and an interactive shell.

The major reasons for building ArukaOS are:
- For intellectual challenge, building an OS isn't an easy task as apart from theory, you must be good with programming to get a basic OS right.
- To get started with Kernel Development.
- Brag on the internet about how 1337 I am (if you get the geekspeak XD).
- Learning about the horrors of OS Development and (ironically) whine about it on [my blog](https://pixeltrik.vercel.app/blogs/osdev-lessons).

## "Hey you mentioned that it has multitasking, how can you show that?"
**As of now, I've enabled the demo since the OS lacks basic commands and programs to play around with and yes, the above GIF is the demonstration.**

Considering it's a command line based OS, it's hard to do that and I've not implmented a spawner yet that can do it gracefully in userland. However, there is a small hack I made in the kernel that you must use to get it activated.
In `src/kernel.c`, go to to `line 162` and flick the `use_demo` demo flag to 
```
bool use_demo = true;
```

And then rebuild the OS by
```
prompt@host$ ./build.sh && ./run-os
```

To see how the demo has been constructed, you can refer `kernel_multi_demo()` function in `src/kernel.c`, it's hacky at best but as I said, I want to make a better implementation for process spawning from user land.

**So for demonstration, this will work.**

## Building and Testing the project
To run this project, you need to run this script.
```
prompt@host$ ./run-os
```

### What do I need to build this project?
This one is straight forward as I build some scripts to assist me to avoid re-typing commands. 
Script `build.sh` will compile the OS, provided **a cross-compiler for i686-elf is available.**  If you don't have one, please refer to [this article](https://wiki.osdev.org/GCC_Cross-Compiler) as you will need one to compile the project.

To build this project you need following dependencies
- `i686-elf-gcc` compiler, it'll be better to build it yourself. [Use this article to know how to do it](https://wiki.osdev.org/GCC_Cross-Compiler).
- `qemu` for x86 processors. 
    - Use `sudo apt-get install -y qemu-system-i386` for Ubuntu based distros
    - Use `yes | sudo pacman -S qemu-arch-extra` for Arch based distros
- GNU Debugger (`gdb`) to debug all the messy modifications you made. **You'll pay dearly for not using it. Trust me, OS development is not very benign with your stupidy.**

### Debugging the project
For this project, **I'm using GNU Debugger for debugging work** as it is one of the most powerful debuggers I know on Linux.

To Debug the project, please use `gdb` in your Unix environment to get into debugging environment.
- Add symbols for the code using 
```
(gdb) add-symbol-file build/kernel-full.o 0x100000
```
- Add all the necessary break points.
- Run the environment using 
```
(gdb) target remote | ./debug-os
```
- Press `c` to continue and good luck debugging the project!

In case you're unfamiliar with using GNU Debugger, you can refer [this article on OS Dev Wiki](https://wiki.osdev.org/How_Do_I_Use_A_Debugger_With_My_OS#Use_GDB_with_QEMU).

## Features Implemented
- Interrupt Descriptor Table
- Programmable Interrupt Controller
- Memory Management
- Disk Streaming (supports only 1 disk)
- File System (read only)
- Keyboard Input (PS/2 based)
- ELF Loader (only static linking has been implemented)
- A basic C library
- A shell that can run programs with command line arguments
- Round robin style multitasking (as shown above)

## What's next?
This is the "Hello World" equivalent of OS world as you can do barely anything in it. **For now, I'll add a bunch of read only commands** but I'll reduce the speed of working on it as I'm kinda exhausted with this project. **OS development is a tedious but satisfying undertaking and while I like it, I don't want to burn myself out.**

To get new ideas and implement them, I better take a break from this and pursue other projects to learn more and possibly implement it in this OS later down the line. This project may get abandoned since this is 
not a serious undertaking.

**In short, I don't know where this will go...**
