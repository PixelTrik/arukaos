#ifndef ARUKA_STDLIB_H
#define ARUKA_STDLIB_H

#include <stddef.h>

void* malloc(size_t sise);
void  free(void* ptr);
char* itoa(int x);

#endif
