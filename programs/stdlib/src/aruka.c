#include "aruka.h"
#include "string.h"

int aruka_getkeyblk() {
  int res = 0;
  do {
    res = aruka_getkey();
  } while(res == 0);

  return res;
}

void aruka_terminal_readline(char* out, int size, bool output_while_typing) {
  int x = 0;
  for (x = 0; x < size; x++) {
    char key = aruka_getkeyblk();

    // For case of Enter key
    if (key == 13)
      break;

    if (output_while_typing)
      aruka_putchar(key);

    // For Backspace
    if (key == 0x08 && x >= 1) {
      out[x - 1] = 0x00;
      x -= 2;
      continue;
    }
    out[x] = key;
  }

  out[x] = 0x00;
}

struct cmd_arg* aruka_parse_cmd(const char* cmd, int max) {
  struct cmd_arg* root = 0;
  char scmd[1025];
  
  if(max >= sizeof(scmd))
    return 0;

  strncpy(scmd, cmd, sizeof(scmd));
  char* token = strtok(scmd, " ");
  if (!token)
    goto out;

  root = aruka_malloc(sizeof(struct cmd_arg));
  if (!root)
    goto out;

  strncpy(root->arg, token, sizeof(root->arg));
  root->next = 0;
  token = strtok(NULL, " ");

  struct cmd_arg* curr = root;
  while (token) {
    struct cmd_arg* n_arg = aruka_malloc(sizeof(struct cmd_arg));
    if (!n_arg)
      break;

    strncpy(n_arg->arg, token, sizeof(n_arg->arg));
    n_arg->next = 0x00;
    curr->next = n_arg;
    curr = n_arg;
    token = strtok(NULL, " ");
  }
out:
  return root;
}

int aruka_sys_run(const char* cmd) {
  char buff[1024];
  strncpy(buff, cmd, sizeof(buff));
  struct cmd_arg* root = aruka_parse_cmd(buff, sizeof(buff));
  if (!root)
    return -1;
  
  return aruka_sys(root);
}
