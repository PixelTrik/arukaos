#ifndef ARUKA_STDIO_H
#define ARUKA_STDIO_H

int putchar(int ch);
int printf(const char *fmt, ...);

#endif
