#include "aruka.h"

extern int main(int argc, char** argv);

void prg_start() {
  struct process_args args;
  aruka_fetch_args(&args);
  int res = main(args.argc, args.argv);

  // For the future, we'll handle "res" to see how the program went
  if (res == 0) {
  }
}
