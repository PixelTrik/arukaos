[BITS 32]

section .asm

; For reference, interrupt <0x80> invokes kernel commands

global aruka_exit:    function
global aruka_print:   function
global aruka_getkey:  function
global aruka_malloc:  function
global aruka_free:    function
global aruka_putchar: function
global aruka_exec:    function
global aruka_sys:     function
global aruka_fetch_args:  function

; void aruka_exit();
aruka_exit:
  push ebp
  mov ebp, esp
  mov eax, 0          ; Kernel Command = exit()
  int 0x80
  pop ebp
  ret

; void aruka_print(const char* msg);
aruka_print:
  push ebp
  mov ebp, esp
  push dword[ebp + 8] ; 4 bytes are for ebp (pointer to function) other 4 bytes are for pointers to arguments
  mov eax, 1          ; Kernel Command = print()
  int 0x80
  add esp, 4          ; Move back stack pointer to clean it up
  pop ebp
  ret

; int aruka_getkey();
aruka_getkey:
  push ebp
  mov ebp, esp
  mov eax, 2          ; Kernel Command = getkey()
  int 0x80
  pop ebp
  ret

; void* aruka_malloc(size_t size);
aruka_malloc:
  push ebp
  mov ebp, esp
  mov eax, 4          ; Kernel Command = malloc()
  push dword[ebp + 8] ; Push argument "size"
  int 0x80
  add esp, 4
  pop ebp
  ret

; void aruka_free(void* ptr);
aruka_free:
  push ebp
  mov ebp, esp
  mov eax, 5          ; Kernel Command = free()
  push dword[ebp + 8] ; Push argument "ptr"
  int 0x80
  add esp, 4
  pop ebp
  ret

; void aruka_putchar(char ch)
aruka_putchar:
  push ebp
  mov ebp, esp
  mov eax, 3          ; Kernel Command = putchar()
  push dword[ebp + 8] ; Push argument "ch"
  int 0x80
  add esp, 4
  pop ebp
  ret

; void aruka_exec(const char* filename);
aruka_exec:
  push ebp
  mov ebp, esp
  mov eax, 6          ; Kernel Command = exec()
  push dword[ebp + 8] ; Push argument "filename"
  int 0x80
  add esp, 4
  pop ebp
  ret

; int aruka_sys(const cmd_arg* args);
aruka_sys:
  push ebp
  mov ebp, esp
  mov eax, 7          ; Kernel Command = sys()
  push dword[ebp + 8] ; Push argument "args"
  int 0x80
  add esp, 4
  pop ebp
  ret

; void aruka_fetch_args(struct process_args* args);
aruka_fetch_args:
  push ebp
  mov ebp, esp
  mov eax, 8          ; Kernel Command = fetch_args()
  push dword[ebp + 8] ; Push argument "args"
  int 0x80
  add esp, 4
  pop ebp
  ret
