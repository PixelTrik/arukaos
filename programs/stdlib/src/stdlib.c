#include "stdlib.h"
#include "aruka.h"

void* malloc(size_t size) {
  return aruka_malloc(size);
}

void free(void* ptr) {
  return aruka_free(ptr);
}

// This is "I to A", converting an integer to ASCII equivalent
char* itoa(int x) {
  // 2^31 ~ 2.14 x 10^9
  // This will give us enough buffer to deal with the conversion.
  
  static char text[12];
  text[11] = 0;
  char neg = ((x < 0) ? 1 : 0);
  int loc = 11;
  x = ((x < 0) ? -x : x);

  while (x) {
    text[--loc] = '0' + (x % 10);
    x /= 10;
  }

  if (loc == 11)
    text[--loc] = '0';

  if (neg)
    text[--loc] = '-';

  return (text + loc);
}
