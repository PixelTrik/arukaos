#ifndef ARUKA_STRING_H
#define ARUKA_STRING_H

#include <stdbool.h>

int   strlen(const char* str);
bool  is_digit(char ch);
int   to_digit(char ch);
int   strnlen(const char* file_path, int max_len);
char* strcpy(char* dest, const char* src);

int istrncmp(const char* str1, const char* str2, int max_len);
int strncmp(const char* str1, const char* str2, int max_len);
char to_lower(char ch);
int strnlen_term(const char* str, int max_len, char term);
char* strncpy(char* dst, const char* src, int len);
char* strtok(char* str, const char* delims);
char* strcat(char* dst, const char* src);

#endif
