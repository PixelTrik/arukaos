#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

void* memset(void* ptr, int ch, size_t size);
int   memcmp(void* ptr1, void* ptr2, int count);
void* memcpy(void* dst, void* src, int len);

#endif
