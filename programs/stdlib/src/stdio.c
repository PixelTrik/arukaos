#include <stdarg.h>

#include "stdlib.h"
#include "stdio.h"
#include "aruka.h"

int putchar(int ch) {
  aruka_putchar((char)(ch));
  return 0;
}

int printf(const char *fmt, ...) {
  va_list ap;
  const char* ptr;
  char* sval;
  int ival;

  va_start(ap, fmt);

  for(ptr = fmt; *ptr; ptr++) {
    if (*ptr != '%') {
      putchar(*ptr);
      continue;
    }

    switch (*(++ptr)) {
      case 'i':
        ival = va_arg(ap, int);
        aruka_print(itoa(ival));
        break;

      case 's':
        sval = va_arg(ap, char*);
        aruka_print(sval);
        break;

      default:
        putchar(*ptr);
    }
  }
  
  va_end(ap);
  return 0;
}
