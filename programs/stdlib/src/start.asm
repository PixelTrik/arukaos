[BITS 32]

section .asm

global _start

extern prg_start
extern aruka_exit

_start:
  call prg_start
  call aruka_exit
  ret
