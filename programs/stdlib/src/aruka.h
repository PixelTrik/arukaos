#ifndef ARUKA_H
#define ARUKA_H

#include <stddef.h>
#include <stdbool.h>

struct cmd_arg {
  char arg[512];
  struct cmd_arg* next;
};

struct process_args {
  int argc;
  char** argv;
};

void aruka_exit();

void aruka_print(const char* msg);
int aruka_getkey();

void* aruka_malloc(size_t size);
void aruka_free(void* ptr);

int aruka_sys_run(const char* cmd);

void aruka_putchar(char ch);
int  aruka_getkeyblk();
void aruka_terminal_readline(char* out, int size, bool output_while_typing);
void aruka_exec(const char* filename);
struct cmd_arg* aruka_parse_cmd(const char* cmd, int max);
int aruka_sys(const struct cmd_arg* args);
void aruka_fetch_args(struct process_args* args);

#endif
