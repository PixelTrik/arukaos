#include "shell.h"
#include "stdio.h"
#include "stdlib.h"
#include "aruka.h"

int main(int argc, char** argv) {
  aruka_print("ArukaOS v0.0.1\n");

  while(1) {
    aruka_print("> ");
    char buff[1024];
    aruka_terminal_readline(buff, sizeof(buff), true);
    aruka_print("\n");

    aruka_sys_run(buff);
  }

  return 0;
}
