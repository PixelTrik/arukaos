#include "process.h"
#include "task/task.h"
#include "task/process.h"
#include "string/string.h"
#include "status.h"
#include "config.h"
#include "kernel.h"

void* isr80h_exec(struct interrupt_frame* frame) {
  void* filename_usrptr = task_get_stack_item(task_current(), 0);
  char filename[ARUKAOS_MAX_PATH];
  int res = copy_string_from_task(task_current(), filename_usrptr, filename, sizeof(filename));
  if (res < 0)
    goto out;

  char path[ARUKAOS_MAX_PATH];
  strcpy(path, "0:/");
  strcpy(path + 3, filename);
  
  struct process* process = 0;
  res = process_load_switch(path, &process);
  if (res < 0)
    goto out;

  task_switch(process->task);
  task_return(&process->task->registers);

out:
  return 0;
}

void* isr80h_invoke_cmd(struct interrupt_frame* frame) {
  struct cmd_arg* args = task_virt_to_phys(task_current(), task_get_stack_item(task_current(), 0));
  if(!args || strlen(args->arg) == 0)
    return ERROR(-EINVARG);

  struct cmd_arg* root_arg = args;
  const char* prg_name = root_arg->arg;
  
  char path[ARUKAOS_MAX_PATH];
  strcpy(path, "0:/");
  strncpy(path + 3, prg_name, sizeof(path));

  struct process* process = 0;
  int res = process_load_switch(path, &process);
  if (res < 0)
    return ERROR(res);

  res = process_inject_args(process, root_arg);
  if (res < 0)
    return ERROR(res);

  task_switch(process->task);
  task_return(&process->task->registers);

  return 0;
}

void* isr80h_fetch_args(struct interrupt_frame* frame) {
  struct process* process = task_current()->process;
  struct process_args* args = task_virt_to_phys(task_current(), task_get_stack_item(task_current(), 0));
  process_get_args(process, &args->argc, &args->argv);

  return 0;
}

void* isr80h_exit(struct interrupt_frame* frame) {
  process_terminate(task_current()->process);
  task_next();

  return 0;
}
