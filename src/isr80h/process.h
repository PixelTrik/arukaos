#ifndef ISR80H_PROCESS_H
#define ISR80H_PROCESS_H

struct interrupt_frame;
void* isr80h_exec(struct interrupt_frame* frame);
void* isr80h_invoke_cmd(struct interrupt_frame* frame);
void* isr80h_fetch_args(struct interrupt_frame* frame);
void* isr80h_exit(struct interrupt_frame* frame);

#endif
