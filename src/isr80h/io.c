#include "io.h"
#include "task/task.h"
#include "kernel.h"
#include "keyboard/keyboard.h"

void* isr80h_print(struct interrupt_frame* frame) {
  void* user_space_buff = task_get_stack_item(task_current(), 0);
  char kernel_buff[1024];
  
  /* We can't just copy the message and print it as the virtual and physical
   * addresses between kernel and user lands aren't resolved.
  */

  copy_string_from_task(task_current(), user_space_buff, kernel_buff, sizeof(kernel_buff)); 
  print(kernel_buff, 15);
  return 0;
}

void* isr80h_getkey(struct interrupt_frame* frame) {
  return (void*)((int)(keyboard_pop()));
}

void* isr80h_putchar(struct interrupt_frame* frame) {
  char ch = (char)(int)(task_get_stack_item(task_current(), 0));
  term_writechar(ch, 15);
  return 0;
}
