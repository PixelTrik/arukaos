#include "isr80h.h"
#include "idt/idt.h"
#include "misc.h"
#include "io.h"
#include "heap.h"
#include "process.h"

void isr80h_register_commands() {
  isr80h_register_command(COMMAND0_SUM,     isr80h_exit);
  isr80h_register_command(COMMAND1_PRINT,   isr80h_print);
  isr80h_register_command(COMMAND2_GETKEY,  isr80h_getkey);
  isr80h_register_command(COMMAND3_PUTCHAR, isr80h_putchar);
  isr80h_register_command(COMMAND4_MALLOC,  isr80h_malloc);
  isr80h_register_command(COMMAND5_FREE,    isr80h_free);
  isr80h_register_command(COMMAND6_EXEC,    isr80h_exec);  
  isr80h_register_command(COMMAND7_INVOKE_CMD, isr80h_invoke_cmd);
  isr80h_register_command(COMMAND8_FETCH_ARGS, isr80h_fetch_args);
}
