#ifndef MISC_H
#define MISC_H

struct interrupt_frame;
void* isr80h_sum(struct interrupt_frame* frame);

#endif
