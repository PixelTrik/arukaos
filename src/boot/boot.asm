ORG 0x7C00
BITS 16

CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

jmp short start
nop

OEMIdentifier           db 'ARUKAOS '
BytesPerSector          dw 0x200
SectorsPerCluster       db 0x80
ReservedSectors         dw 200
FATCopies               db 0x02
RootDirEntries          dw 0x40
NumSectors              dw 0x00
MediaType               db 0xF8
SectorsPerFat           dw 0x100
SectorsPerTrack         dw 0x20
NumberOfHeads           dw 0x40
HiddenSectors           dd 0x00
SectorsBig              dd 0x773594

; Extended BPB (Dos 4.0)
DriveNumber             db 0x80
WinNTBit                db 0x00
Signature               db 0x29
VolumeID                dd 0xD105
VolumeIDString          db 'ARUKAOS BOO'
SystemIDString          db 'FAT16'

start:
  jmp 0: boot

boot:
  cli                 ; Clear interrupts
  ; Critical Work here
  mov ax, 0x00
  mov ds, ax
  mov es, ax
  mov ss, ax
  mov sp, 0x7c00

  ; Critical Work done
  sti                 ; Set interrupts

.load_protected:
  cli
  lgdt[gdt_desc]
  mov eax, cr0
  or eax, 0x1
  mov cr0, eax

  jmp CODE_SEG:load32

; GDT

gdt_start:
gdt_null:
  dd 0x00
  dd 0x00

; offset 0x8
gdt_code:   ; CS should print this
  dw 0xffff       ; Segment limit of first 15 bits
  dw 0x0          ; Base value of first 15 bits
  db 0x0          ; Base value of 16-23
  db 0x9a         ; Access Byte
  db 11001111b    ; High nibble and low nibble
  db 0x0

; offset 0x10
gdt_data:   ; DS, ES, FS, GS
  dw 0xffff       ; Segment limit of first 15 bits
  dw 0x0          ; Base value of first 15 bits
  db 0x0          ; Base value of 16-23
  db 0x92         ; Access Byte
  db 1100111b     ; High nibble and low nibble
  db 0x0
  
gdt_end:

gdt_desc:
  dw gdt_end - gdt_start-1
  dd gdt_start

[BITS 32]
load32:
  mov eax, 1
  mov ecx, 100
  mov edi, 0x0100000
  call ata_lba_read

  jmp CODE_SEG:0x0100000

; Dummy Driver
ata_lba_read:
  mov ebx, eax  ; Backup LBA
  shr eax, 24   ; Keep the most significant 8 bits.
  or eax, 0xE0
  mov dx, 0x1F6
  out dx, al    ; Send the most significant 8 bits.

  ; Send the total sectors to read
  mov eax, ecx
  mov dx, 0x1F2
  out dx, al
  
  ; Send more bits of LBA
  mov eax, ebx
  mov dx, 0x1F3
  out dx, al

  mov dx, 0x1F4
  mov eax, ebx
  shr eax, 8
  out dx, al

  ; Send upper 16 bits of LBA
  mov dx, 0x1F5
  mov eax, ebx
  shr eax, 16
  out dx, al

  mov dx, 0x1F7
  mov al, 0x20
  out dx, al

  ; Read all sectors into memory

.next_sector:
  push ecx

; Check if we want to read
.try_again:
  mov dx, 0x1F7
  in al, dx
  test al, 8
  jz .try_again   ; Loop until ZF != 0

  ; Read 256 words at a time
  mov ecx, 256
  mov dx, 0x1F0
  rep insw
  pop ecx
  loop .next_sector

  ret

times 510-($ - $$) db 0 ; Fill 510 bytes of data by padding zeros
dw 0xAA55               ; Add the boot signature (x86 is little endian, bits are flipped)
