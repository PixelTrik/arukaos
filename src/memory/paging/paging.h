#ifndef PAGING_H
#define PAGING_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define PAGING_CACHE_DISABLED   0b00010000
#define PAGING_WRITE_THROUGH    0b00001000
#define PAGING_UNIV_ACCESS      0b00000100
#define PAGING_IS_WRITEABLE     0b00000010
#define PAGING_IS_PRESENT       0b00000001

#define PAGING_MAX_ENTRIES      1024
#define PAGING_PAGE_SIZE        4096

struct paging_chunk {
  uint32_t* dir_entry;
};

struct paging_chunk* paging_new_chunk(uint8_t flags);
void paging_switch(struct paging_chunk* dir);
void enable_paging();

int paging_set(uint32_t* dir, void* virt, uint32_t val);
bool paging_is_aligned(void* addr);
void paging_free_chunk(struct paging_chunk* chunk);

void* paging_align_addr(void* ptr);
int paging_map(struct paging_chunk* dict, void* virt, void* phys, int flags);
int paging_map_range(struct paging_chunk* dict, void* virt, void* phys, int count, int flags);
int paging_map_to(struct paging_chunk* dict, void* virt, void* phys, void* phys_end, int flags);
uint32_t paging_get(uint32_t* dir, void* virt);
void* paging_align_to_lower_page(void* addr);
void* paging_get_phys_addr(uint32_t* dir, void* virt);

#endif
