#include "paging.h"
#include "memory/heap/kheap.h"
#include "status.h"

void paging_load_dir(uint32_t* dir);

static uint32_t* curr_dir = 0;

struct paging_chunk* paging_new_chunk(uint8_t flags) {
  uint32_t* new_dir = kzalloc(sizeof(uint32_t) * PAGING_MAX_ENTRIES);
  int offset = 0;

  for (int idx = 0; idx < PAGING_MAX_ENTRIES; idx++) {
    uint32_t* entry = kzalloc(sizeof(uint32_t) * PAGING_MAX_ENTRIES);
    for (int b = 0; b < PAGING_MAX_ENTRIES; b++)
      entry[b] = (offset + (b * PAGING_PAGE_SIZE)) | flags;

    offset += (PAGING_MAX_ENTRIES * PAGING_PAGE_SIZE);
    new_dir[idx] = (uint32_t)(entry) | flags | PAGING_IS_WRITEABLE;
  }

  struct paging_chunk* new_chunk = kzalloc(sizeof(struct paging_chunk));
  new_chunk->dir_entry = new_dir;
  return new_chunk;
}

void paging_free_chunk(struct paging_chunk* chunk) {
  for (int x = 0; x < 1024; x++){
    uint32_t  entry = chunk->dir_entry[x];
    uint32_t* table = (uint32_t*)(entry & 0xFFFFF000);
    kfree(table);
  }

  kfree(chunk->dir_entry);
  kfree(chunk);
}

void paging_switch(struct paging_chunk* dir) {
  paging_load_dir(dir->dir_entry);
  curr_dir = dir->dir_entry;
}

bool paging_is_aligned(void* addr) {
  return ((uint32_t)(addr) % PAGING_PAGE_SIZE) == 0;
}

int paging_get_indeces(void* virt, uint32_t* dir_out, uint32_t* tab_out) {
  int res = 0;
  if (!paging_is_aligned(virt)) {
    res = -EINVARG;
    goto out;
  }

  *dir_out = ((uint32_t)(virt) / (PAGING_MAX_ENTRIES * PAGING_PAGE_SIZE));
  *tab_out = ((uint32_t)(virt) % (PAGING_MAX_ENTRIES * PAGING_PAGE_SIZE)) / PAGING_PAGE_SIZE;

out:
  return res;
}

void* paging_align_addr(void* ptr) {
  if ((uint32_t)(ptr) % PAGING_PAGE_SIZE)
    return (void*)(((uint32_t)(ptr) + PAGING_PAGE_SIZE) - ((uint32_t)(ptr) % PAGING_PAGE_SIZE));

  return ptr;
}

void* paging_align_to_lower_page(void* addr) {
  uint32_t _addr = (uint32_t)(addr);
  return (void*)(_addr - (_addr % PAGING_PAGE_SIZE));
}

int paging_map(struct paging_chunk* dict, void* virt, void* phys, int flags) {
  if (((unsigned int)(virt) % PAGING_PAGE_SIZE) || ((uint32_t)(phys) % PAGING_PAGE_SIZE))
    return -EINVARG;

  return paging_set(dict->dir_entry, virt, (uint32_t)(phys) | flags);
}

int paging_map_range(struct paging_chunk* dict, void* virt, void* phys, int count, int flags) {
  int res = 0;
  for (int x = 0; x < count; x++) {
    res = paging_map(dict, virt, phys, flags);
    if (res < 0)
      break;
    
    virt += PAGING_PAGE_SIZE;
    phys += PAGING_PAGE_SIZE;
  }

  return res;
}

int paging_map_to(struct paging_chunk* dict, void* virt, void* phys, void* phys_end, int flags) {
  int res = 0;
  if (((uint32_t)(virt) % PAGING_PAGE_SIZE) || ((uint32_t)(phys) %
        PAGING_PAGE_SIZE) || ((uint32_t)(phys_end) % PAGING_PAGE_SIZE) ||
      (uint32_t)(phys_end) < (uint32_t)(phys)) 
    return -EINVARG;

  uint32_t bytes = phys_end - phys;
  int pages = bytes / PAGING_PAGE_SIZE;
  res = paging_map_range(dict, virt, phys, pages, flags);

  return res;
}

int paging_set(uint32_t* dir, void* virt, uint32_t val) {
  if (!paging_is_aligned(virt))
    return -EINVARG;

  uint32_t dir_idx = 0, tab_idx = 0;
  int res = paging_get_indeces(virt, &dir_idx, &tab_idx);
  if (res < 0)
    return res;

  uint32_t  entry = dir[dir_idx];
  uint32_t* table = (uint32_t*)(entry & 0xfffff000);
  table[tab_idx] = val;

  return 0;
}

uint32_t paging_get(uint32_t* dir, void* virt) {
  // This will return the physical address of the virtual address "virt"
  uint32_t dir_idx = 0, tab_idx = 0;
  paging_get_indeces(virt, &dir_idx, &tab_idx);
  uint32_t  entry = dir[dir_idx];
  uint32_t* table = (uint32_t*)(entry & 0xfffff000);
  return table[tab_idx];
}

void* paging_get_phys_addr(uint32_t* dir, void* virt) {
  void* virt_new = paging_align_to_lower_page(virt);
  void* diff = (void*)((uint32_t)(virt) - (uint32_t)(virt_new));
  return (void*)((paging_get(dir, virt_new) & 0xfffff000) + diff);
}
