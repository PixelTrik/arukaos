#include "memory.h"

void* memset(void* ptr, int ch, size_t size) {
  char* c_ptr = (char*) ptr;
  for (size_t x = 0; x < size; x++)
    *(c_ptr + x) = (char) ch;
  return ptr;
}

int memcmp(void* ptr1, void* ptr2, int count) {
  char* ch1 = (char*)(ptr1);
  char* ch2 = (char*)(ptr2);

  while ((count--) > 0)
    if ((*ch1++) != (*ch2++))
      return (*(ch1 - 1) < *(ch2 - 1) ? -1 : 1);

  return 0;
}

void* memcpy(void* dst, void* src, int len) {
  char* d = dst;
  char* s = src;
  
  while(len--)
    *d++ = *s++;

  return dst;
}
