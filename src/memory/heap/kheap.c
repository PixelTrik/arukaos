#include "kheap.h"
#include "heap.h"
#include "config.h"
#include "kernel.h"
#include "memory/memory.h"

struct heap       kernel_heap;
struct heap_table kernel_heap_table;

void kheap_init() {
  int total_entries = ARUKAOS_HEAP_SIZE / ARUKAOS_HEAP_BLK_SIZE;

  kernel_heap_table.entries = (heap_blk_entry*) (ARUKAOS_HEAP_TABLE_ADDR);
  kernel_heap_table.total   = total_entries;

  void* end = (void*) (ARUKAOS_HEAP_ADDR + ARUKAOS_HEAP_SIZE);
  int res = heap_create(&kernel_heap, (void*)(ARUKAOS_HEAP_ADDR), end, &kernel_heap_table);
  if (res < 0)
    print("Failed to create heap!", 13);
}

void* kmalloc(size_t size) {
  return heap_malloc(&kernel_heap, size);
}

void* kzalloc(size_t size) {
  void* ptr = kmalloc(size);
  if (!ptr)
    return 0;

  memset(ptr, 0x00, size);
  return ptr;
}

void kfree(void* ptr) {
  heap_free(&kernel_heap, ptr);
}
