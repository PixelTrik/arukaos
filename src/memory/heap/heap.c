#include "heap.h"
#include "kernel.h"
#include "status.h"
#include "memory/memory.h"

static int heap_validate_table(void* ptr, void* end, struct heap_table* table) {
  int res = 0;

  size_t table_size   = (size_t)(end - ptr);
  size_t total_pages  = table_size / ARUKAOS_HEAP_BLK_SIZE;

  if (table->total != total_pages) {
    res = -EINVARG;
    goto out;
  }
out:
  return res;
}

static int heap_validate_alignment(void* ptr) {
  return ((unsigned int) ptr % ARUKAOS_HEAP_BLK_SIZE) == 0;
}

int heap_create(struct heap* heap, void* ptr, void* end, struct heap_table* table) {
  int res = 0;

  if (!heap_validate_alignment(ptr) || !heap_validate_alignment(end)) {
    res = -EINVARG;
    goto out;
  }

  memset(heap, 0, sizeof(struct heap));

  heap->start_addr  = ptr;
  heap->table       = table;

  res = heap_validate_table(ptr, end, table);

  if (res < 0)
    goto out;

  size_t table_size = sizeof(heap_blk_entry) * table->total;
  memset(table->entries, HEAP_BLK_IS_FREE, table_size);

out:
  return res;
}

static uint32_t heap_align_value(uint32_t val) {
  if ((val % ARUKAOS_HEAP_BLK_SIZE) == 0)
    return val;

  val -= (val % ARUKAOS_HEAP_BLK_SIZE);
  val += ARUKAOS_HEAP_BLK_SIZE;

  return val;
}

void* heap_blk_to_addr (struct heap* heap, int block) {
  return (heap->start_addr + (block * ARUKAOS_HEAP_BLK_SIZE));
}

int heap_get_start_block (struct heap* heap, uint32_t blk_count) {
  struct heap_table* table = heap->table;
  int blk_curr = 0, blk_start = -1;

  for (size_t x = 0; x < table->total; x++) {
    if (table->entries[x] != HEAP_BLK_IS_FREE) {
      // Reset Values if the chain of blocks isn't continuous
      blk_curr  = 0;
      blk_start = -1;
      continue;
    }
    
    if (blk_start == -1)
      blk_start = x;
    blk_curr++;

    if (blk_curr == blk_count)
      break;
  }

  return ((blk_start == -1) ? -ENOMEM : blk_start);
}

void heap_mark_blk_taken(struct heap* heap, int start_blk, uint32_t blk_count) {
  int end_blk = start_blk + blk_count - 1;
  heap_blk_entry entry = HEAP_BLK_IS_TAKEN | HEAP_BLK_IS_FIRST;

  if (blk_count > 1)
    entry |= HEAP_BLK_HAS_N;

  for (int x = start_blk; x <= end_blk; x++) {
    heap->table->entries[x] = entry;
    entry = HEAP_BLK_IS_TAKEN;
    
    if (x != end_blk - 1)
      entry |= HEAP_BLK_HAS_N;
  }
}

void* heap_malloc_blocks(struct heap* heap, uint32_t blk_count) {
  void* addr = 0;
  int start_blk = heap_get_start_block(heap, blk_count);

  if (start_blk < 0)
    goto out;

  addr = heap_blk_to_addr(heap, start_blk);
  // Mark the blocks as taken
  heap_mark_blk_taken(heap, start_blk, blk_count);
out:
  return addr;
}

void* heap_malloc(struct heap* heap, size_t size) {
  size_t aligned_size = heap_align_value(size);
  uint32_t total_blocks = aligned_size / ARUKAOS_HEAP_BLK_SIZE;

  return heap_malloc_blocks(heap, total_blocks);
}

void heap_free(struct heap* heap, void* ptr) {
  size_t addr = (size_t)(ptr - heap->start_addr) / ARUKAOS_HEAP_BLK_SIZE;
  struct heap_table* table = heap->table;

  for (size_t x = addr; x < table->total; x++) {
    heap_blk_entry entry = table->entries[x];
    table->entries[x] = HEAP_BLK_IS_FREE;
    
    if (!(entry & HEAP_BLK_HAS_N))
      break;
  }
}
