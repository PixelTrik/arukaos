#ifndef HEAP_H
#define HEAP_H

#include <stddef.h>
#include <stdint.h>

#include "config.h"

#define HEAP_BLK_HAS_N    0x40
#define HEAP_BLK_IS_FIRST 0x80
#define HEAP_BLK_IS_TAKEN 0x01
#define HEAP_BLK_IS_FREE  0x00

typedef unsigned char heap_blk_entry;

struct heap_table {
  heap_blk_entry*   entries;
  size_t            total;
};

struct heap {
  struct heap_table*  table;
  void*               start_addr;
};

int heap_create(struct heap* heap, void* ptr, void* end, struct heap_table* table);
void* heap_malloc(struct heap* heap, size_t size);
void heap_free(struct heap* heap, void* ptr);
#endif
