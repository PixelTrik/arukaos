#include "file.h"
#include "config.h"
#include "status.h"
#include "fat/fat16.h"
#include "disk/disk.h"
#include "kernel.h"
#include "memory/memory.h"
#include "memory/heap/kheap.h"

struct filesystem* filesystems[ARUKAOS_MAX_FILESYSTEMS];
struct file_descriptor* file_descriptors[ARUKAOS_MAX_FILE_DESCRIPTORS];

static struct filesystem** fs_get_free_filesystem() {
  int x = 0;
  for (x = 0; x < ARUKAOS_MAX_FILESYSTEMS; x++)
    if (!filesystems[x])
      return &filesystems[x];

  return 0;
}

void fs_insert_filesystem(struct filesystem* filesystem) {
  struct filesystem** fs;
  fs = fs_get_free_filesystem();
  
  if (!fs) {
    print("\nProblem inserting filesystem...\n", 1);
    while(1) {}
  }

  *fs = filesystem;
}

static void fs_static_load() { 
  fs_insert_filesystem(fat16_init());
}

void fs_load() {
  memset(filesystems, 0x00, sizeof(filesystems));
  fs_static_load();
}

void fs_init() {
  memset(file_descriptors, 0x00, sizeof(file_descriptors));
  fs_load();
}

static int file_new_descriptor(struct file_descriptor** desc_out) {
  int res = -ENOMEM;
  for (int x = 0; x < ARUKAOS_MAX_FILE_DESCRIPTORS; x++) {
    if (file_descriptors[x] == 0) {
      struct file_descriptor* desc = kzalloc(sizeof(struct file_descriptor));
      desc->idx = x + 1;
      file_descriptors[x] = desc;
      *desc_out = desc;
      res = 0;
      break;
    }
  }

  return res;
}

static struct file_descriptor* file_get_descriptor(int fd) {
  if (fd <= 0 || fd >= ARUKAOS_MAX_FILE_DESCRIPTORS)
    return 0;

  int idx = fd - 1;
  return file_descriptors[idx];
}

struct filesystem* fs_resolve(struct disk* disk) {
  struct filesystem* fs = 0;
  for (int x = 0; x < ARUKAOS_MAX_FILESYSTEMS; x++) {
    if (filesystems[x] != 0 && filesystems[x]->resolve(disk) == 0) {
      fs = filesystems[x];
      break;
    }
  }

  return fs;
}

file_mode file_get_mode(const char* str) {
  
  switch(str[0]) {
    case 'r' : return FILE_MODE_READ;
    case 'w' : return FILE_MODE_WRITE;
    case 'a' : return FILE_MODE_APPEND;
  }

  return FILE_MODE_INVALID;
}

int fopen(const char* filename, const char* mode_str) {
  int res = 0;
  struct path_root* root = path_parser_parse(filename, NULL);
  if (!root) {
    res = -EINVARG;
    goto out;
  }

  /* If there's nothing but an empty root directory, then exit. 
   * We cannot have just a root path.
   * This will be detected by the filesystem but why bother when you know it
   * right now?
  */
  if (!root->first) {
    res = -EINVARG;
    goto out;
  }

  // Check for existing disk in the path
  struct disk* disk = disk_get(root->drive_no);
  if (!disk) {
    res = -EIO;
    goto out;
  }

  if (!disk->filesystem) {
    res = -EIO;
    goto out;
  }

  file_mode mode = file_get_mode(mode_str);
  if (mode == FILE_MODE_INVALID) {
    res = -EINVARG;
    goto out;
  }

  void* descriptor_private_data = disk->filesystem->open(disk, root->first, mode);
  if (ISERR(descriptor_private_data)) {
    res = ERROR_I(descriptor_private_data);
    goto out;
  }

  struct file_descriptor* desc = 0;
  res = file_new_descriptor(&desc);
  if (res < 0)
    goto out;

  desc->filesystem = disk->filesystem;
  desc->private = descriptor_private_data;
  desc->disk = disk;
  res = desc->idx;

out:
  return ((res < 0) ? 0 : res);
}

int fstat(int fd, struct file_stat* stat) {
  int res = 0;
  struct file_descriptor* desc = file_get_descriptor(fd);
  if (!desc) {
    res = -EIO;
    goto out;
  }

  res = desc->filesystem->stat(desc->disk, desc->private, stat);

out:
  return res;
}

int fclose(int fd) {
  int res = 0;
  struct file_descriptor* desc = file_get_descriptor(fd);
  if (!desc) {
    res = -EIO;
    goto out;
  }

  res = desc->filesystem->close(desc->private);
  if (res == ARUKAOS_ALL_OK) {
    file_descriptors[desc->idx - 1] = 0x00;
    kfree(desc);
  }

out:
  return res;
}

int fseek(int fd, int offset, file_seek_mode whence) {
  int res = 0;
  struct file_descriptor* desc = file_get_descriptor(fd);
  if (!desc) {
    res = -EIO;
    goto out;
  }

  res = desc->filesystem->seek(desc->private, offset, whence);

out:
  return res;
}

int fread(void* ptr, uint32_t size, uint32_t nmemb, int fd) {
  int res = 0;
  if (size == 0 || nmemb == 0 || fd < 1) {
    res = -EINVARG;
    goto out;
  }

  struct file_descriptor* desc = file_get_descriptor(fd);
  if (!desc) {
    res = -EINVARG;
    goto out;
  }

  res = desc->filesystem->read(desc->disk, desc->private, size, nmemb, (char*) ptr);

out:
  return res;
}
