#include "pparser.h"
#include "kernel.h"
#include "status.h"
#include "string/string.h"
#include "memory/heap/kheap.h"
#include "memory/memory.h"

static int path_parser_valid_format(const char* file_path) {
  int len = strnlen(file_path, ARUKAOS_MAX_PATH);
  return (len >= 3 && is_digit(file_path[0]) && memcmp((void*)(file_path + 1), ":/", 2) == 0);
}

static int path_parser_get_drive_by_path(const char** path) {
  if (!path_parser_valid_format(*path))
    return -EBADPATH;

  int drive_no = to_digit(*path[0]);
  *path += 3;

  return drive_no;
}

struct path_root* path_parser_gen_root(int drive_no) {
  struct path_root* new_root = kzalloc(sizeof(struct path_root));

  new_root->drive_no  = drive_no;
  new_root->first     = 0;

  return new_root;
}

static const char* path_parser_get_path_part(const char** path) {
  char* res_part = kzalloc(ARUKAOS_MAX_PATH);
  int x = 0;
  while (**path != '/' && **path != 0x00) {
    res_part[x++] = **path;
    *path += 1;
  }

  if (**path == '/')
    *path += 1;

  if (x == 0) {
    kfree(res_part);
    res_part = 0;
  }

  return res_part;
}

struct path_part* path_parser_parse_path_part(struct path_part* last, const char** path) {
  const char* part_str = path_parser_get_path_part(path);
  if (!part_str)
    return 0;

  struct path_part* part = kzalloc(sizeof(struct path_part));
  part->part = part_str;
  part->next = 0x00;

  if (last)
    last->next = part;

  return part;
}

void path_parser_free(struct path_root* root) {
  struct path_part* part = root->first;
  
  while (part) {
    struct path_part* next_part = part->next;
    kfree((void*)(part->part));
    kfree((void*)(part));
    part = next_part;
  }

  kfree((void*)(root));
}

struct path_root* path_parser_parse(const char* path, const char* curr_dir) {
  int res = 0;
  const char* tmp_path = path;
  struct path_root* root = 0;

  if (strlen(tmp_path) > ARUKAOS_MAX_PATH)
    goto out;

  res = path_parser_get_drive_by_path(&tmp_path);
  if (res < 0)
    goto out;

  root = path_parser_gen_root(res);
  if (!root)
    goto out;

  struct path_part* first_path = path_parser_parse_path_part(NULL, &tmp_path);
  if (!first_path)
    goto out;

  root->first = first_path;

  struct path_part* part = path_parser_parse_path_part(first_path, &tmp_path);
  while (part)
    part = path_parser_parse_path_part(part, &tmp_path);

out:
  return root;
}
