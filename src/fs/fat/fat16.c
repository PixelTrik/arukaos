#include <stdint.h>

#include "fat16.h"
#include "string/string.h"
#include "disk/disk.h"
#include "disk/stream.h"
#include "status.h"
#include "memory/memory.h"
#include "memory/heap/kheap.h"
#include "kernel.h"

#define ARUKAOS_FAT16_SIGNATURE       0x29
#define ARUKAOS_FAT16_FAT_ENTRY_SIZE  0x02
#define ARUKAOS_FAT16_BAD_SECTOR      0xFF7
#define ARUKAOS_FAT16_UNUSED          0x00

typedef unsigned int fat_item_type;
#define FAT_ITEM_TYPE_DIR             0
#define FAT_ITEM_TYPE_FILE            1

#define FAT_FILE_READ_ONLY            0x01
#define FAT_FILE_HIDDEN               0x02
#define FAT_FILE_SYSTEM               0x04
#define FAT_FILE_VOLUME_LABEL         0x08
#define FAT_FILE_SUBDIRECTORY         0x10
#define FAT_FILE_ARCHIVED             0x20
#define FAT_FILE_DEVICE               0x40
#define FAT_FILE_RESERVED             0x80

struct fat_header_extended {

  uint8_t   drive_no;
  uint8_t   win_nt_bit;
  uint8_t   signature;
  uint32_t  vol_id;
  uint8_t   vol_id_str[11];
  uint8_t   sys_id_str[8];

} __attribute__((packed));

struct fat_header {

  uint8_t   short_jmp_ins[3];
  uint8_t   oem_id[8];
  uint16_t  bytes_per_sector;
  uint8_t   sectors_per_cluster;
  uint16_t  reserved_sectors;
  uint8_t   fat_copies;
  uint16_t  root_dir_entries;
  uint16_t  number_of_sectors;
  uint8_t   media_type;
  uint16_t  sectors_per_fat;
  uint16_t  sectors_per_track;
  uint16_t  number_of_heads;
  uint32_t  hidden_sectors;
  uint32_t  sectors_big;

} __attribute__((packed));

struct fat_h {
  struct fat_header primary_header;
  union {
    struct fat_header_extended extended_header;
  } shared;
};

struct fat_directory_item {

  uint8_t   filename[8];
  uint8_t   ext[3];
  uint8_t   attribute;
  uint8_t   reserved;
  uint8_t   creation_time_tenth;
  uint16_t  creation_time;
  uint16_t  creation_date;
  uint16_t  last_access;
  uint16_t  first_cluster_1; // high bits
  uint16_t  last_mod_time;
  uint16_t  last_mod_date;
  uint16_t  first_cluster_2; // low bits
  uint32_t  filesize;

}__attribute__((packed));

struct fat_directory {
  struct fat_directory_item* item;
  int total;
  int sector_pos;
  int end_sector_pos;
};

struct fat_item {
  union {
    struct fat_directory_item*  item;
    struct fat_directory*       directory;
  };

  fat_item_type type;
};

struct fat_file_descriptor {
  struct fat_item*  item;
  uint32_t          pos;
};

struct fat_private {
  struct fat_h          header;
  struct fat_directory  root_directory;

  struct disk_stream*   cluster_read_stream;
  struct disk_stream*   fat_read_stream;
  struct disk_stream*   directory_stream;
};

int fat16_resolve(struct disk* disk);
int fat16_read(struct disk* disk, void* desc, uint32_t size, uint32_t nmemb, char* out_ptr);
void* fat16_open(struct disk* disk, struct path_part* path, file_mode mode);
int fat16_seek(void* private, int offset, file_seek_mode mode);
int fat16_stat(struct disk* disk, void* desc_priv, struct file_stat* stat);
int fat16_close(void* private);

struct filesystem fat16_fs = {
  .resolve  = fat16_resolve,
  .open     = fat16_open,
  .read     = fat16_read,
  .seek     = fat16_seek,
  .stat     = fat16_stat,
  .close    = fat16_close
};

struct filesystem* fat16_init() {
  strcpy(fat16_fs.name, "FAT16");
  return &fat16_fs;
}

void fat16_init_private(struct disk* disk, struct fat_private* private) {
  memset(private, 0x00, sizeof(struct fat_private));

  private->cluster_read_stream  = disk_stream_new(disk->id);
  private->fat_read_stream      = disk_stream_new(disk->id);
  private->directory_stream     = disk_stream_new(disk->id);
}

int fat16_sec_to_abs(struct disk* disk, int sector) {
  return sector * disk->sector_size;
}

int fat16_get_total_items(struct disk* disk, uint32_t dir_start_sec) {
  struct fat_directory_item item, empty_item;
  memset(&empty_item, 0x00, sizeof(empty_item));

  struct fat_private* private = disk->fs_private;
  int res = 0, count = 0, start_pos = dir_start_sec * disk->sector_size;
  struct disk_stream* stream = private->directory_stream;
  // Move the stream pointer to directory's entry
  if (disk_stream_seek(stream, start_pos) != ARUKAOS_ALL_OK) {
    res = -EIO;
    goto out;
  }

  while (1) {
    // Checking all entries in the directory

    if (disk_stream_read(stream, &item, sizeof(item)) != ARUKAOS_ALL_OK) {
      res = -EIO;
      goto out;
    }

    // Case of a blank record, we're done
    if (*item.filename == 0x00)
      break;

    // Case of directory entry is free, there are still more entries, so
    // continue
    if (*item.filename == 0xE5)
      continue;

    count++;
  }

  res = count;
out:
  return res;
}

int fat16_get_root_directory(struct disk* disk, struct fat_private* fat_private, struct fat_directory* dir_out) {
  int res = 0;
  struct fat_header* primary_header = &fat_private->header.primary_header;
  struct fat_directory_item* dir = 0x00;
  // This will give us the first sector that can be addressed since those
  // directories come after fat copies and reserved sectors
  int sector_pos = (primary_header->fat_copies * primary_header->sectors_per_fat) + primary_header->reserved_sectors;
  int entries = primary_header->root_dir_entries;
  int size = entries * sizeof(struct fat_directory_item);
  int total_sectors = size / disk->sector_size;
  if (size % disk->sector_size)
    ++total_sectors;

  int total_items = fat16_get_total_items(disk, sector_pos);

  dir = kzalloc(size);
  if (!dir) {
    res = -ENOMEM;
    goto out;
  }

  struct disk_stream* stream = fat_private->directory_stream;
  if (disk_stream_seek(stream, fat16_sec_to_abs(disk, sector_pos)) != ARUKAOS_ALL_OK) {
    // We fail to seek the parts of the disk we were supposed to read
    res = -EIO;
    goto out;
  }

  if (disk_stream_read(stream, dir, size) != ARUKAOS_ALL_OK) {
    res = -EIO;
    goto out;
  }

  dir_out->item = dir;
  dir_out->total = total_items;
  dir_out->sector_pos = sector_pos;
  dir_out->end_sector_pos = sector_pos + (size / disk->sector_size);

  return res;

out:
  if (dir)
    kfree(dir);

  return res;
}

int fat16_resolve(struct disk* disk) {
  int res = 0;
  // Initializing private data of FAT16 filsystem to use it as primary
  // reference for various tasks like veerification, caching, etc.
  struct fat_private* fat_private = kzalloc(sizeof(struct fat_private));
  fat16_init_private(disk, fat_private);

  // The filesystem is now bound to the disk
  disk->fs_private = fat_private;
  disk->filesystem = &fat16_fs;

  struct disk_stream* stream = disk_stream_new(disk->id);
  if (!stream) {
    res = -ENOMEM;
    goto out;
  }

  if (disk_stream_read(stream, &fat_private->header, sizeof(fat_private->header)) != ARUKAOS_ALL_OK) {
    res = -EIO;
    goto out;
  }

  if (fat_private->header.shared.extended_header.signature != 0x29) {
    /*
     * This boot signature indicates that Volume ID, Label and filesystem type
     * are present in the header. If not present, we cannot risk reading from
     * an unknown file system and potentially corrupt data.
     */
    res = -EFSNOTUS;
    goto out;
  }

  if (fat16_get_root_directory(disk, fat_private, &fat_private->root_directory) != ARUKAOS_ALL_OK) {
    res = -EIO;
    goto out;
  }

out:
  if (stream)
    disk_stream_close(stream);

  if (res < 0) {
    // Unbind the problematic filesystem.
    kfree(fat_private);
    disk->fs_private = 0;
  }

  return res;
}

static uint32_t fat16_get_first_cluster(struct fat_directory_item* item) {
  return (uint32_t)((item->first_cluster_1) | (item->first_cluster_2));
}

static int fat16_cluster_to_sector(struct fat_private* private, int cluster) {
  return private->root_directory.end_sector_pos + ((cluster - 2) * private->header.primary_header.sectors_per_cluster);
}

static uint32_t fat16_get_first_fat_sector(struct fat_private* private) {
  // The first sector in a FAT filesystem comes after reserved sectors.
  return private->header.primary_header.reserved_sectors;
}

void fat16_to_proper_string(char** out, const char* in, size_t size) {
  size_t x = 0;

  while (*in != 0x00 && *in != 0x20) {
    **out = *in;
    *out += 1;
    in += 1;

    if (x >= size - 1) 
      break;

    x++;
  }

  **out = 0x00;
}

void fat16_get_full_relative_filename(struct fat_directory_item* item, char* out, int len) {
  memset(out, 0x00, len);
  char* out_tmp = out;
  fat16_to_proper_string(&out_tmp, (const char*)(item->filename), sizeof(item->filename));
  if (*item->ext != 0x00 && *item->ext != 0x20) {
    *out_tmp++ = '.';
    fat16_to_proper_string(&out_tmp, (const char*)(item->ext), sizeof(item->ext));
  }
}

struct fat_directory_item* fat16_clone_directory_item(struct fat_directory_item* item, int size) {
  struct fat_directory_item* copy = 0;
  if (size < sizeof(struct fat_directory_item))
    return 0;

  copy = kzalloc(size);
  if (!copy)
    return 0;

  memcpy(copy, item, size);
  return copy;
}

static int fat16_get_fat_entry(struct disk* disk, int cluster) {
  int res = -1;
  struct fat_private* private = disk->fs_private;
  struct disk_stream* stream = private->fat_read_stream;
  if (!stream)
    goto out;

  uint32_t fat_pos = fat16_get_first_fat_sector(private) * disk->sector_size;
  res = disk_stream_seek(stream, fat_pos * (cluster * ARUKAOS_FAT16_FAT_ENTRY_SIZE));
  if (res < 0)
    goto out;

  uint16_t result = 0;
  res = disk_stream_read(stream, &result, sizeof(result));
  if (res < 0)
    goto out;

  res = result;

out:
  return res;
}

static int fat16_get_cluster(struct disk* disk, int cluster, int offset) {
  int res = 0;

  struct fat_private* private = disk->fs_private;
  int cluster_size = private->header.primary_header.sectors_per_cluster * disk->sector_size;
  int curr_cluster = cluster;
  int next_clusters = offset / cluster_size;

  for (int x = 0; x < next_clusters; x++) {
    int entry = fat16_get_fat_entry(disk, curr_cluster);
    // Check to avoid last entry in file
    if (entry == 0xFF8 || entry == 0xFFF) {
      res = -EIO;
      goto out;
    }

    // Check to avoid bad sectors
    if (entry == ARUKAOS_FAT16_BAD_SECTOR) {
      res = -EIO;
      goto out;
    }

    // Check to avoid reserved sectors
    if (entry == 0xFF0 || entry == 0xFF6) {
      res = -EIO;
      goto out;
    }

    // If this is true, the FAT is corrupted
    if (entry == 0x00) {
      res = -EIO;
      goto out;
    }

    curr_cluster = entry;
  }

  res = curr_cluster;
out:
  return res;
}

static int fat16_read_internal_from_stream(struct disk* disk, struct disk_stream* stream, int cluster, int offset, int total, void* out) {
  int res = 0;
  struct fat_private* private = disk->fs_private;
  int cluster_size = private->header.primary_header.sectors_per_cluster * disk->sector_size;
  int curr_cluster = fat16_get_cluster(disk, cluster, offset);
  if (curr_cluster < 0) {
    res = curr_cluster;
    goto out;
  }

  int cluster_offset = offset % cluster_size;
  int start_sector = fat16_cluster_to_sector(private, curr_cluster);
  int start_pos = (start_sector * disk->sector_size) + cluster_offset;
  int total_to_read = (total > cluster_size) ? cluster_size : total;
  res = disk_stream_seek(stream, start_pos);
  if (res != ARUKAOS_ALL_OK)
    goto out;

  res = disk_stream_read(stream, out, total_to_read);
  if (res != ARUKAOS_ALL_OK)
    goto out;

  total -= total_to_read;
  if (total > 0)
    res = fat16_read_internal_from_stream(disk, stream, cluster, offset + total_to_read, total, out + total_to_read);

out:
  return res;
}

static int fat16_read_internal(struct disk* disk, int cluster, int offset, int total, void* out) {
  struct fat_private* private = disk->fs_private;
  struct disk_stream* cluster_stream = private->cluster_read_stream;
  return fat16_read_internal_from_stream(disk, cluster_stream, cluster, offset, total, out);
}

void fat16_free_directory(struct fat_directory* directory) {
  if (!directory)
    return;

  if (directory->item)
    kfree(directory->item);

  kfree(directory);
}

void fat16_free_item(struct fat_item* item) {
  if (item->type == FAT_ITEM_TYPE_DIR)
    fat16_free_directory(item->directory);
  else if (item->type == FAT_ITEM_TYPE_FILE)
    kfree(item->item);

  kfree(item);
}

struct fat_directory* fat16_load_fat_directory(struct disk* disk, struct fat_directory_item* item) {
  int res = 0;
  struct fat_directory* directory = 0;
  struct fat_private* private = disk->fs_private;
  if (!(item->attribute & FAT_FILE_SUBDIRECTORY)) {
    res = -EINVARG;
    goto out;
  }

  directory = kzalloc(sizeof(struct fat_directory));
  if (!directory) {
    res = -ENOMEM;
    goto out;
  }

  int cluster = fat16_get_first_cluster(item);
  int cluster_sector = fat16_cluster_to_sector(private, cluster);
  int total_items = fat16_get_total_items(disk, cluster_sector);
  directory->total = total_items;
  int dir_size = directory->total * sizeof(struct fat_directory_item);
  directory->item = kzalloc(dir_size);
  if (!directory->item) {
    res = -ENOMEM;
    goto out;
  }

  res = fat16_read_internal(disk, cluster, 0x00, dir_size, directory->item);
  if(res != ARUKAOS_ALL_OK)
    goto out;

out:
  if (res != ARUKAOS_ALL_OK)
    fat16_free_directory(directory);

  return directory;
}

struct fat_item* fat16_new_fat_item_for_directory_item(struct disk* disk, struct fat_directory_item* item) {
  struct fat_item* f_item = kzalloc(sizeof(struct fat_item));
  if (!f_item)
    return 0;

  if (item->attribute & FAT_FILE_SUBDIRECTORY) {
    f_item->directory = fat16_load_fat_directory(disk, item);
    f_item->type = FAT_ITEM_TYPE_DIR;
    return f_item;
  }

  f_item->type = FAT_ITEM_TYPE_FILE;
  f_item->item = fat16_clone_directory_item(item, sizeof(struct fat_directory_item));
  return f_item;
}

struct fat_item* fat16_find_item_in_directory(struct disk* disk, struct fat_directory* directory, const char* name) {
  struct fat_item* item = 0;
  char tmp_name[ARUKAOS_MAX_PATH];

  for (int x = 0; x < directory->total; x++) {
    fat16_get_full_relative_filename(directory->item + x, tmp_name, sizeof(tmp_name));
    if (istrncmp(tmp_name, name, sizeof(tmp_name)) == 0)
      item = fat16_new_fat_item_for_directory_item(disk, directory->item + x);
  }

  return item;
}

struct fat_item* fat16_get_directory_entry(struct disk* disk, struct path_part* path) {
  struct fat_private* private = disk->fs_private;
  struct fat_item* curr_item = 0;
  struct fat_item* root_item = fat16_find_item_in_directory(disk, &private->root_directory, path->part);
  if (!root_item)
    goto out;

  struct path_part* next_part = path->next;
  curr_item = root_item;

  while (next_part) {
    if (curr_item->type != FAT_ITEM_TYPE_DIR) {
      curr_item = 0;
      break;
    }

    struct fat_item* tmp_item = fat16_find_item_in_directory(disk, curr_item->directory, next_part->part);
    fat16_free_item(curr_item);
    curr_item = tmp_item;
    next_part = next_part->next;
  }

out:
  return curr_item;
}

void* fat16_open(struct disk* disk, struct path_part* path, file_mode mode) {
  struct fat_file_descriptor* desc = 0x00;
  int err_code = 0;

  if (mode != FILE_MODE_READ) {
    err_code = -ERDONLY;
    goto out;
  }

  desc = kzalloc(sizeof(struct fat_file_descriptor));
  if (!desc) {
    err_code = -ENOMEM;
    goto out;
  }

  desc->item = fat16_get_directory_entry(disk, path);
  if (!desc->item) {
    err_code = -EIO;
    goto out;
  }

  desc->pos = 0;
  return desc;

out:
  if (desc)
    kfree(desc);

  return ERROR(err_code);
}

int fat16_close(void* private) {
  struct fat_file_descriptor* desc = private;

  fat16_free_item(desc->item);
  kfree(desc);

  return 0;
}

int fat16_stat(struct disk* disk, void* desc_priv, struct file_stat* stat) {
  int res = 0;
  struct fat_file_descriptor* desc = desc_priv;
  if (desc->item->type != FAT_ITEM_TYPE_FILE) {
    res = -EINVARG;
    goto out;
  }

  struct fat_directory_item* item = desc->item->item;
  stat->filesize = item->filesize;
  stat->flags = 0x00;

  if (item->attribute & FAT_FILE_READ_ONLY)
    stat->flags |= FILE_STAT_READ_ONLY;

out:
  return res;
}

int fat16_read(struct disk* disk, void* desc, uint32_t size, uint32_t nmemb, char* out_ptr) {
  int res = 0;
  struct fat_file_descriptor* fat_desc = desc;
  struct fat_directory_item* item = fat_desc->item->item;
  int offset = fat_desc->pos;

  for (uint32_t x = 0; x < nmemb; x++) {
    res = fat16_read_internal(disk, fat16_get_first_cluster(item), offset, size, out_ptr);
    if (ISERR(res))
      goto out;

    out_ptr += size;
    offset += size;
  }

  res = nmemb;
out:
  return res;
}

int fat16_seek(void* private, int offset, file_seek_mode mode) {
  int res = 0;
  struct fat_file_descriptor* desc = private;
  struct fat_item* desc_item = desc->item;
  if (desc_item->type != FAT_ITEM_TYPE_FILE) {
    res = -EINVARG;
    goto out;
  }

  struct fat_directory_item* item = desc_item->item;
  if (offset >= item->filesize) {
    res = -EIO;
    goto out;
  }

  switch(mode) {
    case SEEK_SET:
      desc->pos = offset;
      break;

    case SEEK_END:
      res = -EUNIMP;
      break;

    case SEEK_CUR:
      desc->pos += offset;
      break;

    default:
      res = -EINVARG;
  }

out:
  return res;
}
