#ifndef FILE_H
#define FILE_H

#include <stdint.h>

#include "pparser.h"

typedef unsigned int file_seek_mode;

enum {
  SEEK_SET,
  SEEK_CUR,
  SEEK_END
};

typedef unsigned int file_mode;
enum {
  FILE_MODE_READ,
  FILE_MODE_WRITE,
  FILE_MODE_APPEND,
  FILE_MODE_INVALID
};

enum {
  FILE_STAT_READ_ONLY = 0x01
};

typedef unsigned int file_state_flags;

struct file_stat {
  file_state_flags flags;
  uint32_t filesize;
};

struct disk;
typedef void*(*FS_OPEN_FUNCTION)(struct disk* disk, struct path_part* path, file_mode mode);
typedef int (*FS_READ_FUNCTION)(struct disk* disk, void* private, uint32_t size, uint32_t nmemb, char* ptr);
typedef int (*FS_RESOLVE_FUNCTION)(struct disk* disk);
typedef int (*FS_SEEK_FUNCTION)(void* private, int offset, file_seek_mode whence);
typedef int (*FS_STAT_FUNCTION)(struct disk* disk, void* private, struct file_stat* stat);
typedef int (*FS_CLOSE_FUNCTION)(void* private);

struct filesystem {
  FS_RESOLVE_FUNCTION resolve;
  FS_OPEN_FUNCTION    open;
  FS_READ_FUNCTION    read;
  FS_SEEK_FUNCTION    seek;
  FS_STAT_FUNCTION    stat;
  FS_CLOSE_FUNCTION   close;

  char name[20];
};

struct file_descriptor {
  int idx;
  struct filesystem* filesystem;
  void* private;
  struct disk* disk;
};

void fs_init();
void fs_insert_filesystem(struct filesystem* filesystem);

int fopen(const char* filename, const char* mode_str);
int fread(void* ptr, uint32_t size, uint32_t nmemb, int fd);
int fseek(int fd, int offset, file_seek_mode whence);
int fstat(int fd, struct file_stat* stat);
int fclose(int fd);
struct filesystem* fs_resolve(struct disk* disk);

#endif
