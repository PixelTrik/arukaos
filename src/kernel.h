#ifndef KERNEL_H
#define KERNEL_H

#define VGA_WIDTH         80
#define VGA_HEIGHT        20

#define ARUKAOS_MAX_PATH  108

#define ERROR(value)   (void*)(value)
#define ERROR_I(value) (int)(value)
#define ISERR(value)   ((int)(value) < 0)

void kernel_main(); 
void print(const char* str, char color);
void term_writechar(char ch, char col);
void panic(const char* msg);
void kernel_page();
void kernel_registers();

#endif
