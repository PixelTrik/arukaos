[BITS 32]
section .asm

global restore_general_registers
global task_return
global user_registers

; void task_return(struct registers* regs);
task_return:
  mov ebp, esp
  mov ebx, [ebp + 4]

  ; Pushing
  ; 1. Data Segment
  ; 2. Stack Address
  ; 3. Flags
  ; 4. Code Segment
  ; 5. Instruction Pointer

  mov ebx, [ebp + 4]
  push dword [ebx + 44]   ; Data/Stack Selector - pushed
  push dword [ebx + 40]   ; Stack Pointer - pushed
  pushf                   ; Flags - pushed
  pop eax
  or eax, 0x200
  push eax
  push dword [ebx + 32]   ; Code Segment - pushed
  push dword [ebx + 28]   ; Instruction Pointer - pushed

  ; Setting up segment registers

  mov ax, [ebx + 44]
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax

  push dword [ebp + 4]
  call restore_general_registers
  add esp, 4
  iretd

; void restore_general_registers(struct registers* regs)
restore_general_registers:
  push ebp
  mov ebp, esp

  mov ebx, [ebp + 8]
  mov edi, [ebx]
  mov esi, [ebx + 4]
  mov ebp, [ebx + 8]
  mov eax, [ebx + 12]
  mov ecx, [ebx + 20]
  mov edx, [ebx + 24]
  mov ebx, [ebx + 16]

  add esp, 4
  ret

; void user_registers();
user_registers:
  mov ax, 0x23
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  ret
