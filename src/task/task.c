#include "task.h"
#include "kernel.h"
#include "status.h"
#include "process.h"
#include "memory/heap/kheap.h"
#include "memory/memory.h"
#include "memory/paging/paging.h"
#include "idt/idt.h"
#include "string/string.h"
#include "loader/elfloader.h"

struct task* curr_task = 0;

// Task Linked List
struct task* task_tail = 0;
struct task* task_head = 0;

int task_init(struct task* task, struct process* process);

struct task* task_current() {
  return curr_task;
}

struct task* task_get_next() {
  return ((curr_task->next) ? curr_task->next : task_head);
}

struct task* task_new(struct process* process) {
  int res = 0;
  struct task* task = kzalloc(sizeof(struct task));
  if (!task) {
    res = -ENOMEM;
    goto out;
  }

  res = task_init(task, process);
  if (res != ARUKAOS_ALL_OK)
    goto out;

  if (task_head == 0) {
    task_head = task_tail = curr_task = task;
    goto out;
  }

  task_tail->next = task;
  task->prev      = task_tail;
  task_tail       = task;

out:
  if (ISERR(res)) {
    task_free(task);
    return ERROR(res);
  }

  return task;
}

static void task_list_remove(struct task* task) {
  if (task->prev)
    task->prev->next = task->next;

  if (task->next)
    task->next->prev = task->prev;

  if (task == task_head)
    task_head = task->next;

  if (task == task_tail)
    task_tail = task->prev;

  if (task == curr_task)
    curr_task = task_get_next();
}

int task_free(struct task* task) {
  paging_free_chunk(task->page_dir);
  task_list_remove(task);
  kfree(task);
  return 0;
}

void task_next() {
  struct task* next = task_get_next();
  if (!next)
    panic("No more tasks!!!\n");

  task_switch(next);
  task_return(&next->registers);
}

int task_switch(struct task* task) {
  curr_task = task;
  paging_switch(task->page_dir);
  return 0;
}

int copy_string_from_task(struct task* task, void* virt, void* phys, int max) {
  if (max >= PAGING_PAGE_SIZE)
    return -EINVARG;

  int res = 0;
  char* tmp = kzalloc(max); // This buffer will be shared by kernel & task page tables.
  if (!tmp)
    return -ENOMEM;

  uint32_t* task_dir = task->page_dir->dir_entry;
  uint32_t old_entry = paging_get(task_dir, tmp);
  paging_map(task->page_dir, tmp, tmp, PAGING_IS_WRITEABLE | PAGING_IS_PRESENT | PAGING_UNIV_ACCESS);
  paging_switch(task->page_dir);
  strncpy(tmp, virt, max);
  kernel_page();

  res = paging_set(task_dir, tmp, old_entry);
  if (res < 0) {
    res = -EIO;
    goto out;
  }

  strncpy(phys, tmp, max);

out:
  kfree(tmp);
  return res;
}

void task_save_current_state(struct interrupt_frame* frame) {
  if (!curr_task)
    panic("No current task to save!");

  struct task* task = curr_task;

  // Saving the current state
  task->registers.ip    = frame->ip;
  task->registers.cs    = frame->cs;
  task->registers.flags = frame->flags;
  task->registers.esp   = frame->esp;
  task->registers.ss    = frame->ss;
  task->registers.eax   = frame->eax;
  task->registers.ebp   = frame->ebp;
  task->registers.ebx   = frame->ebx;
  task->registers.ecx   = frame->ecx;
  task->registers.edi   = frame->edi;
  task->registers.edx   = frame->edx;
  task->registers.esi   = frame->esi;
}

int task_page() {
  user_registers();
  task_switch(curr_task);
  return 0;
}

int task_page_task(struct task* task) {
  user_registers();
  paging_switch(task->page_dir);
  return 0;
}

void task_first_task() {
  if (!curr_task)
    panic("task_first_task(): No current task!");

  task_switch(task_head);
  task_return(&task_head->registers);
}

int task_init(struct task* task, struct process* process) {
  memset(task, 0x00, sizeof(struct task));
  task->page_dir = paging_new_chunk(PAGING_IS_PRESENT | PAGING_UNIV_ACCESS);
  if (!task->page_dir)
    return -EIO;

  task->registers.ip  = ARUKAOS_PROGRAM_VIRTUAL_ADDR;
  
  if (process->filetype == PROCESS_FILETYPE_ELF)
    task->registers.ip = elf_header(process->elf_file)->e_entry;

  task->registers.ss  = USER_DATA_SEGMENT;
  task->registers.cs  = USER_CODE_SEGMENT;
  task->registers.esp = ARUKAOS_PROGRAM_VIRTUAL_STACK_ADDR_START;
  task->process       = process;

  return 0;
}

void* task_get_stack_item(struct task* task, int idx) {
  void* res = 0;
  uint32_t* stack_ptr = (uint32_t*)(task->registers.esp);
  task_page_task(task); // Switching the task's page

  res = (void*)(stack_ptr[idx]);

  kernel_page(); // Switching back to kernel page
  return res;
}

void* task_virt_to_phys(struct task* task, void* virt) {
  return paging_get_phys_addr(task->page_dir->dir_entry, virt);
}
