#include "kernel.h"
#include "process.h"
#include "task.h"
#include "config.h"
#include "status.h"
#include "fs/file.h"
#include "memory/memory.h"
#include "string/string.h"
#include "memory/heap/kheap.h"
#include "memory/paging/paging.h"
#include "loader/elfloader.h"

struct process* curr_process = 0; // Current running process
static struct process* processes[ARUKAOS_MAX_PROCESSES] = {};

static void process_init(struct process* process) {
  memset(process, 0x00, sizeof(struct process));
}

struct process* process_current() {
  return curr_process;
}

struct process* process_get(int process_id) {
  return ((process_id < 0 || process_id >= ARUKAOS_MAX_PROCESSES) ? NULL : processes[process_id]);
}

int process_get_free_slot() {
  for (int i = 0; i < ARUKAOS_MAX_PROCESSES; i++)
    if (!processes[i])
      return i;

  return -EISTKN;
}

static int process_load_elf(const char* filename, struct process* process) {
  int res = 0;
  struct elf_file* elf_file = 0;
  res = elf_load(filename, &elf_file);
  if (ISERR(res))
    goto out;

  process->filetype = PROCESS_FILETYPE_ELF;
  process->elf_file = elf_file;

out:
  return res;
}

int process_load(const char* filename, struct process** process) {
  int process_slot = process_get_free_slot();
  
  if (process_slot < 0)
    return -EISTKN;

  return process_load_for_slot(filename, process, process_slot);
}

int process_load_switch(const char* filename, struct process** process) {
  int res = process_load(filename, process);
  if (res == ARUKAOS_ALL_OK)
    process_switch(*process);
  return res;
}

static int process_load_bin(const char* filename, struct process* process) {
  void* prg_data = 0;
  int res = 0, fd = fopen(filename, "r");
  if (!fd) {
    res = -EIO;
    goto out;
  }

  struct file_stat stat;
  res = fstat(fd, &stat);
  if (res != ARUKAOS_ALL_OK)
    goto out;

  prg_data = kzalloc(stat.filesize);
  if (!prg_data) {
    res = -ENOMEM;
    goto out;
  }

  if (fread(prg_data, stat.filesize, 1, fd) != 1) {
    res = -EIO;
    goto out;
  }

  process->filetype = PROCESS_FILETYPE_BIN;
  process->ptr      = prg_data;
  process->size     = stat.filesize;

out:
  if (res < 0) {
    if (prg_data)
      kfree(prg_data);
  }
  fclose(fd);
  return res;
}

static int process_load_data(const char* filename, struct process* process) {
  int res = process_load_elf(filename, process);
  if (res == -EINFORMAT)
    res = process_load_bin(filename, process);
  return res;
}

int process_switch(struct process* process) {
  curr_process = process;
  return 0;
}

int process_map_bin(struct process* process) {
  int res = 0;
  paging_map_to(process->task->page_dir,
      (void*)(ARUKAOS_PROGRAM_VIRTUAL_ADDR), process->ptr,
      paging_align_addr(process->ptr + process->size), PAGING_IS_PRESENT |
      PAGING_IS_WRITEABLE | PAGING_UNIV_ACCESS);
  return res;
}

static int process_map_elf(struct process* process) {
  int res = 0;
  struct elf_file* elf_file = process->elf_file;
  struct elf_header* header = elf_header(elf_file);
  struct elf32_phdr* phdrs  = elf_pheader(header);

  for (int x = 0; x < header->e_phnum; x++) {
    struct elf32_phdr* phdr = phdrs + x;
    void* phdr_phys_addr = elf_phdr_phys_addr(elf_file, phdr);
    int flags = PAGING_IS_PRESENT | PAGING_UNIV_ACCESS;
    if (phdr->p_flags & PF_W)
      flags |= PAGING_IS_WRITEABLE;

    res = paging_map_to(process->task->page_dir,
          paging_align_to_lower_page((void*)(phdr->p_vaddr)),
          paging_align_to_lower_page(phdr_phys_addr),
          paging_align_addr(phdr_phys_addr + phdr->p_memsz), flags);

    if (ISERR(res))
      break;
  }
  return res;
}

int process_map_mem(struct process* process) {
  int res = 0;
  switch (process->filetype) {
    case PROCESS_FILETYPE_ELF: res = process_map_elf(process); break;
    case PROCESS_FILETYPE_BIN: res = process_map_bin(process); break;
    default: panic("process_map_mem: INVALID FILETYPE\n");
  }
  if (res < 0)
    goto out;

  // Remapping the stack
  paging_map_to(process->task->page_dir, (void*)(ARUKAOS_PROGRAM_VIRTUAL_STACK_ADDR_END), process->stack,
      paging_align_addr(process->stack + ARUKAOS_USER_PROGRAM_STACK_SIZE),
      PAGING_IS_PRESENT | PAGING_UNIV_ACCESS | PAGING_IS_WRITEABLE);
  
out:
  return res;
}

int process_load_for_slot(const char* filename, struct process** process, int slot) {
  // Sets the ID for the loaded process
  int res = 0;
  struct task* task = 0;
  struct process* _process = 0;
  void* prg_stack = 0;

  if (process_get(slot) != 0) {
    res = -EISTKN;
    goto out;
  }

  _process = kzalloc(sizeof(struct process));
  if (!process) {
    res = -ENOMEM;
    goto out;
  }

  process_init(_process);
  res = process_load_data(filename, _process);
  if (res < 0)
    goto out;

  prg_stack = kzalloc(ARUKAOS_USER_PROGRAM_STACK_SIZE);
  if (!prg_stack) {
    res = -ENOMEM;
    goto out;
  }

  strncpy(_process->filename, filename, sizeof(_process->filename));
  _process->stack = prg_stack;
  _process->id = slot;

  // Create a task
  task = task_new(_process);
  if (ERROR_I(task) == 0) {
    res = ERROR_I(task);
    goto out;
  }

  _process->task = task;
  res = process_map_mem(_process);
  if (res < 0)
    goto out;

  *process        = _process;
  processes[slot] = _process;

out:
  if (ISERR(res)) {
    if (_process && _process->task)
      task_free(_process->task);

  }
  return res;
}

static int process_find_free_alloc_idx(struct process* process) {
  for(int x = 0; x < ARUKAOS_MAX_PROGRAM_ALLOCATIONS; x++)
    if (!process->allocations[x].ptr)
      return x;

  return -ENOMEM;
}

void* process_malloc(struct process* process, size_t size) {
  void* ptr = kzalloc(size);
  if (!ptr)
    goto out_err;

  int idx = process_find_free_alloc_idx(process);
  if (idx < 0)
    goto out_err;

  // Mapping memory for the newly allocated space.
  int res = paging_map_to(process->task->page_dir, ptr, ptr,
      paging_align_addr(ptr + size), PAGING_IS_WRITEABLE | PAGING_IS_PRESENT |
      PAGING_UNIV_ACCESS);
  if (res < 0)
    goto out_err;

  process->allocations[idx].ptr   = ptr;
  process->allocations[idx].size  = size;

  return ptr;

out_err:
  // Avoiding memory leaks if we can't allocate
  if (ptr)
    kfree(ptr);
  return 0;
}

void process_free(struct process* process, void* ptr) {
// Checking if the process argument owns the allocation. 
  struct process_alloc* alloc = 0;
  for (int x = 0; x < ARUKAOS_MAX_PROGRAM_ALLOCATIONS; x++) {
    if (process->allocations[x].ptr == ptr) {
      alloc = process->allocations + x;
      break;
    }
  }

  if (!alloc)
    return;

  // Since the process owns the allocation, first unmap the memory
  int res = paging_map_to(process->task->page_dir, alloc->ptr, alloc->ptr,
      paging_align_addr(alloc->ptr + alloc->size), 0x00);
  if (res < 0)
    return;

  // Unlink the allocation 
  for (int x = 0; x < ARUKAOS_MAX_PROGRAM_ALLOCATIONS; x++) {
    if(process->allocations[x].ptr == ptr) {
      process->allocations[x].ptr   = 0x00;
      process->allocations[x].size  = 0;
    }
  }

  kfree(ptr);
}

// Injecting command line arguments
void process_get_args(struct process* process, int* argc, char*** argv) {
  *argc = process->args.argc;
  *argv = process->args.argv;
}

int process_count_args(struct cmd_arg* root) {
  struct cmd_arg* curr = root;
  int count = 0;
  while (curr) {
    count++;
    curr = curr->next;
  }

  return count;
}

static void process_unlink(struct process* process) {
  processes[process->id] = 0x00;
  if (process == curr_process) {
    for (int x = 0; x < ARUKAOS_MAX_PROGRAM_ALLOCATIONS; x++) {
      if (processes[x]) {
        process_switch(processes[x]);
        return;
      }  
    }
  }

  panic("No processess to switch. Ideally should've shut down!\n");
}

int process_terminate(struct process* process) {
  // Freeing all allocations made by this process
  // To keep de-allocation safe, we're using process_free()
  for (int x = 0; x < ARUKAOS_MAX_PROGRAM_ALLOCATIONS; x++)
    process_free(process, process->allocations[x].ptr);

  // Freeing program data, before that, we must confirm the filetype
  switch(process->filetype) {
    case PROCESS_FILETYPE_BIN: kfree(process->ptr); break;
    case PROCESS_FILETYPE_ELF: elf_close(process->elf_file); break;
    default: return -EINVARG;
  }

  kfree(process->stack);
  task_free(process->task);
  process_unlink(process);

  return 0;
}

int process_inject_args(struct process* process, struct cmd_arg* root) {
  int res = 0;
  struct cmd_arg* curr = root;
  int x = 0,
      argc = process_count_args(root);
  if (argc == 0) {
    res = -EIO;
    goto out;
  }

  char** argv = process_malloc(process, sizeof(const char*) * argc);
  if (!argv) {
    res = -ENOMEM;
    goto out;
  }

  while (curr) {
    char* str = process_malloc(process, sizeof(curr->arg));
    if (!str) {
      res = -ENOMEM;
      goto out;
    }

    strncpy(str, curr->arg, sizeof(curr->arg));
    argv[x++] = str;
    curr = curr->next;
  }
  
  process->args.argc = argc;
  process->args.argv = argv;

out:
  return res;
}
