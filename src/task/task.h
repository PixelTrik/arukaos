#ifndef TASK_H
#define TASK_H

#include "config.h"
#include "memory/paging/paging.h"

struct registers {
  uint32_t edi;
  uint32_t esi;
  uint32_t ebp;
  uint32_t eax;
  uint32_t ebx;
  uint32_t ecx;
  uint32_t edx;

  uint32_t ip; // Instruction Pointers (also called Program Counter in x86)
  uint32_t cs; // Code Segment of the process in x86
  uint32_t flags;
  uint32_t esp;
  uint32_t ss;
};

struct process;
struct interrupt_frame;

struct task {
  struct paging_chunk* page_dir;
  struct registers registers; // Stores the state of registers when the task is about to get inactive and get switched.
  struct process* process;
  struct task* next;
  struct task* prev;
};

struct task* task_new(struct process* process);
struct task* task_current();
struct task* task_get_next();
int task_free(struct task* task);

int task_switch(struct task* task);
int task_page();

void task_first_task();

void task_return(struct registers* regs);
void user_registers();
void restore_general_registers(struct registers* regs);
void task_save_current_state(struct interrupt_frame* frame);
int copy_string_from_task(struct task* task, void* virt, void* phys, int max);
void* task_get_stack_item(struct task* task, int idx);
int task_page_task(struct task* task);
void* task_virt_to_phys(struct task* task, void* virt);
void task_next();

#endif
