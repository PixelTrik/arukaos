#ifndef PROCESS_H
#define PROCESS_H

#include <stdint.h>
#include <stdbool.h>

#include "task.h"
#include "config.h"

#define PROCESS_FILETYPE_BIN 0
#define PROCESS_FILETYPE_ELF 1

typedef unsigned char process_filetype;

struct process_alloc {
  void*   ptr;
  size_t  size;
};

struct cmd_arg {
  char arg[512];
  struct cmd_arg* next;
};

struct process_args {
  int argc;
  char** argv;
};

struct process {
  uint16_t      id;
  char          filename[ARUKAOS_MAX_PATH];
  struct task*  task;

  // Used to keep track of allocations made by the programmer to prevent memory
  // leaks from program crashing or programmer's studpidity.
  struct process_alloc allocations[ARUKAOS_MAX_PROGRAM_ALLOCATIONS]; 

  process_filetype filetype;

  // Physical Pointers for ELF or binary file
  union {
    void* ptr;
    struct elf_file* elf_file;
  };

  void*     stack;  // Physical pointer to the stack memory
  uint32_t  size;

  struct keyboard_buff {
    char buff[ARUKAOS_KEYBOARD_BUFSIZE];
    int tail;
    int head;
  } keyboard;

  struct process_args args;
};

int process_switch(struct process* process);
int process_load_switch(const char* filename, struct process** process);
int process_load_for_slot(const char* filename, struct process** process, int slot);
int process_load(const char* filename, struct process** process);
struct process* process_current();
struct process* process_get(int process_id);
void* process_malloc(struct process* process, size_t size);
void  process_free(struct process* process, void* ptr);
void process_get_args(struct process* process, int* argc, char*** argv);
int process_inject_args(struct process* process, struct cmd_arg* root);
int process_terminate(struct process* process);

#endif
