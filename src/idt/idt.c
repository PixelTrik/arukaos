#include "idt.h"
#include "config.h"
#include "memory/memory.h"
#include "kernel.h"
#include "io/io.h"
#include "task/task.h"
#include "task/process.h"
#include "status.h"

idt_desc  idt_descriptors[ARUKAOS_INTS];
idtr_desc idtr_descriptor;

static ISR80H_COMMAND isr80h_commands[ARUKAOS_MAX_ISR80H_COMMANDS];
static INTERRUPT_CALLBACK_FUNCTION interrupt_callbacks[ARUKAOS_INTS];

extern void int21h();
extern void no_int();
extern void idt_load(idtr_desc* ptr);
extern void isr80h_wrapper();

extern void* interrupt_pointer_table[ARUKAOS_INTS];

void idt_zero() {
  print("Divide by Zero error!!!\n", 15);
}

void no_int_handler() {
  outb(0x20, 0x20);
}

void interrupt_handler(int interrupt, struct interrupt_frame* frame) {
  kernel_page();

  if (interrupt_callbacks[interrupt]) {
    task_save_current_state(frame);
    interrupt_callbacks[interrupt](frame);
  }

  task_page();
  outb(0x20, 0x20);
}

void idt_set(int int_no, void* addr) {
  idt_desc* desc = idt_descriptors + int_no;

  desc->offset_1  = (uint32_t) addr & 0xffff;
  desc->selector  = KERNEL_CODE_SELECTOR;
  desc->zero      = 0x00;
  desc->type_attr = 0xEE;
  desc->offset_2  = (uint32_t) addr >> 16;
}

void idt_handle_exception() {
  process_terminate(task_current()->process);
  task_next();
}

void idt_clock() {
  outb(0x20, 0x20);
  task_next();
}

void idt_init() {
  memset(idt_descriptors, 0, sizeof(idt_descriptors));

  idtr_descriptor.limit = sizeof(idt_descriptors) - 1;
  idtr_descriptor.base  = (uint32_t) idt_descriptors;

  for (int x = 0; x < ARUKAOS_INTS; x++)
    idt_set(x, interrupt_pointer_table[x]);

  idt_set(0,    idt_zero);
  idt_set(0x80, isr80h_wrapper);

  for (int x = 0; x < 0x20; x++)
    idt_register_interrupt_callback(x, idt_handle_exception);

  idt_register_interrupt_callback(0x20, idt_clock);
  idt_load(&idtr_descriptor);
}

int idt_register_interrupt_callback(int interrupt, INTERRUPT_CALLBACK_FUNCTION callback) {
  if (interrupt < 0 || interrupt >= ARUKAOS_INTS)
    return -EINVARG;

  interrupt_callbacks[interrupt] = callback;
  return 0;
}

void isr80h_register_command(int idx, ISR80H_COMMAND command) {
  if (idx < 0 || idx > ARUKAOS_MAX_ISR80H_COMMANDS)
    panic("Command out of bounds!");

  if (isr80h_commands[idx])
    panic("Attempt to overwrite command");

  isr80h_commands[idx] = command;
}

void* isr80h_handle_command(int command, struct interrupt_frame* frame) {
  if (command < 0 || command > ARUKAOS_MAX_ISR80H_COMMANDS)
    return 0; // Invalid Command

  ISR80H_COMMAND command_func = isr80h_commands[command];
  if (!command_func)
    return 0; // Can't process this command

  return command_func(frame);
}

void* isr80h_handler(int command, struct interrupt_frame* frame) {
  void* res = 0;
  kernel_page();
  task_save_current_state(frame);
  res = isr80h_handle_command(command, frame);
  task_page();
  return res;
}
