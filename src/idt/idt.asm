section .asm

extern no_int_handler 
extern isr80h_handler
extern interrupt_handler

global int21h
global no_int
global enable_interrupts
global disable_interrupts
global idt_load
global isr80h_wrapper
global interrupt_pointer_table
enable_interrupts:
  sti
  ret

disable_interrupts:
  cli
  ret

idt_load:
  push ebp
  mov ebp, esp
  mov ebx, [ebp+8]
  
  lidt [ebx]

  pop ebp
  ret

no_int:
  pushad

  call no_int_handler 

  popad
  iret

%macro interrupt 1
  global int%1
  
  ; Upon calling this interrupt, IP, CS, flags, SP and SS are pushed into the
  ; stack.
  int%1:
    pushad                    ; All general purpose registers are pushed into the stack.
    push esp                  ; The stack pointer is pushed to refer the above frame.
    push dword %1             ; The first argument is the command of the interrupt
    call interrupt_handler
    add esp, 8                ; Restore the stack
    popad
    iret
%endmacro

; This is a for loop
%assign i 0
%rep 512
  interrupt i
%assign i i+1
%endrep

isr80h_wrapper:
  ; Upon calling this interrupt, IP, CS, flags, SP and SS are pushed into the
  ; stack.
  pushad                    ; All general purpose registers are pushed into the stack.

  push esp                  ; The stack pointer is pushed to refer the above frame.
  push eax                  ; EAX holds the command of the current interrupt.
  call isr80h_handler
  mov dword[tmp_res], eax
  add esp, 8                ; Moving the stack pointer back to point at our current frame again.

  popad                     ; Restoring general purpose registers
  mov eax, [tmp_res]
  iretd

section .data
tmp_res: dd 0               ; Temperory storage for result

; Building the interrupt_pointer_table
%macro interrupt_entry 1
  dd int%1
%endmacro

interrupt_pointer_table:
%assign i 0
%rep 512
  interrupt_entry i
%assign i i+1
%endrep
