#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "status.h"
#include "kernel.h"
#include "config.h"
#include "idt/idt.h"
#include "io/io.h"
#include "memory/heap/kheap.h"
#include "memory/paging/paging.h"
#include "memory/memory.h"
#include "disk/disk.h"
#include "fs/pparser.h"
#include "fs/file.h"
#include "string/string.h"
#include "disk/stream.h"
#include "gdt/gdt.h"
#include "task/tss.h"
#include "task/process.h"
#include "task/task.h"
#include "isr80h/isr80h.h"
#include "keyboard/keyboard.h"

uint16_t* video_mem = 0;
uint16_t term_y     = 0; // Current row
uint16_t term_x     = 0; // Current column

static struct paging_chunk* kernel_chunk = 0;

void init_term() {
  video_mem = (uint16_t*) (0xB8000);
  for (uint8_t y = 0; y < VGA_HEIGHT; y++)
    for (uint8_t x = 0; x < VGA_WIDTH; x++)
      video_mem[(y * VGA_WIDTH) + x] = ((0 << 8) | ' ');
}

void term_backspace() {
  if (term_x == 0 && term_y == 0)
    return;

  if (term_x == 0) {
    term_y -= 1;
    term_x = VGA_WIDTH;
  }

  term_x -= 1;
  term_writechar(' ', 0x00);
  term_x -= 1;
}

void term_writechar(char ch, char col) {
  if (ch == '\n') {
    term_x = 0;
    term_y++;
    return;
  }

  if (ch == 0x08) {
    // 0x08 = Backspace Key
    term_backspace();
    return;
  }

  video_mem[(term_y * VGA_WIDTH) + term_x] = ((col << 8) | ch);

  term_x++;

  if (term_x >= VGA_WIDTH) {
    term_x = 0;
    term_y++;
  }
}

void print(const char* str, char color) {
  int len = strlen(str);

  for (int x = 0; x < len; x++)
    term_writechar(str[x], color);
}

void panic(const char* msg) {
  char panic_color = 12;
  print("\n\n======== KERNEL PANIC ========\n\n", panic_color);
  print(msg, panic_color);
  print("\n\n==============================\n\n", panic_color);
  while (1) { }
}

void kernel_page() {
  kernel_registers();
  paging_switch(kernel_chunk);
}

struct tss tss;
struct gdt gdt_real[ARUKAOS_TOTAL_GDT_SEGMENTS];
struct gdt_structured gdt_structured[ARUKAOS_TOTAL_GDT_SEGMENTS] = {
  {.base = 0x00,              .limit = 0x00,        .type = 0x00}, // NULL Segment
  {.base = 0x00,              .limit = 0xFFFFFFFF,  .type = 0x9A}, // Kernel Code Segment 
  {.base = 0x00,              .limit = 0xFFFFFFFF,  .type = 0x92}, // Kernel Data Segment 
  {.base = 0x00,              .limit = 0xFFFFFFFF,  .type = 0xF8}, // User Data Segment 
  {.base = 0x00,              .limit = 0xFFFFFFFF,  .type = 0xF2}, // User Data Segment 
  {.base = (uint32_t)(&tss),  .limit = sizeof(tss), .type = 0xE9}, // Task Segment 
};

void kernel_multi_demo(struct process* process) {
  int res = process_load_switch("0:/blank", &process);
  if (res != ARUKAOS_ALL_OK)
  {
      panic("Failed to load blank\n");
  }

  struct cmd_arg arg;
  strcpy(arg.arg, "Testing!");
  arg.next = 0x00; 

  process_inject_args(process, &arg);

  res = process_load_switch("0:/blank", &process);
  if (res != ARUKAOS_ALL_OK)
  {
      panic("Failed to load blank\n");
  }

  strcpy(arg.arg, "Abc!");
  arg.next = 0x00; 
  process_inject_args(process, &arg);
}

void kernel_main() {
  init_term();
  term_x = term_y = 0;

  memset(gdt_real, 0x00, sizeof(gdt_real));
  gdt_structured_to_gdt(gdt_real, gdt_structured, ARUKAOS_TOTAL_GDT_SEGMENTS);

  // Load the GDT
  gdt_load(gdt_real, sizeof(gdt_real));

  kheap_init();
  fs_init();
  disk_search_and_init();
  idt_init();

  // Setup TSS
  memset(&tss, 0x00, sizeof(tss));
  tss.esp0  = 0x600000; // Sets the stack pointer position in Kernel Land
  tss.ss0   = KERNEL_DATA_SELECTOR;

  // Load TSS
  tss_load(0x28);

  kernel_chunk = paging_new_chunk(PAGING_IS_WRITEABLE | PAGING_IS_PRESENT | PAGING_UNIV_ACCESS);
  paging_switch(kernel_chunk);

  enable_paging();

  isr80h_register_commands();
  keyboard_init();

  struct process* process = 0;
  bool use_demo = true;
  if (use_demo) {
    kernel_multi_demo(process);
    goto run;
  }

  int res = process_load_switch("0:/shell.elf", &process);
  if (res != ARUKAOS_ALL_OK)
    panic("Failed to load \"shell.elf\"\n");

run:
  task_first_task();
  while(1){}

}
