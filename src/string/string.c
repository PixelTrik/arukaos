#include "string.h"

int strnlen(const char* str, int max_len) {
  int x = 0;
  for (x = 0; x < max_len; x++)
    if (str[x] == 0)
      break;

  return x;
}

int strnlen_term(const char* str, int max_len, char term) {
  int len = 0;
  for (len = 0; len < max_len; len++)
    if (str[len] == 0 || str[len] == term)
      break;

  return len;
}

char to_lower(char ch) {
  return (ch >= 65 && ch <= 90) ? ch + 32 : ch;
}

int istrncmp(const char* str1, const char* str2, int max_len) {
  unsigned char u1, u2;
  while ((max_len--) > 0) {
    u1 = (unsigned char)(*str1++);
    u2 = (unsigned char)(*str2++);
    if ((u1 != u2) && (to_lower(u1) != to_lower(u2)))
      return u1 - u2;
    if (u1 == 0)
      return 0;
  }

  return 0;
}

int strncmp(const char* str1, const char* str2, int max_len) {
  unsigned char u1, u2;
  while ((max_len--) > 0) {
    u1 = (unsigned char)(*str1++);
    u2 = (unsigned char)(*str2++);
    if (u1 != u2)
      return u1 - u2;
    if (u1 == 0)
      return 0;
  }

  return 0;
}

int strlen(const char* str) {
  int len = 0;
  while (str[len])
    len++;
  return len;
}

char* strncpy(char* dst, const char* src, int len) {
  int x = 0;
  dst[len] = 0x00;

  for (x = 0; x < len - 1; x++) {
    if (!(src + x))
      break;
    
    dst[x] = src[x];
  }

  return dst;
}

bool is_digit(char ch) {
  return (ch >= 48) && (ch <= 57);
}

int to_digit(char ch) {
  return (int)(ch - 48);
}

char* strcpy(char* dest, const char* src) {
  char* res = dest;

  while (*src != 0)
    *(dest++) = *(src++);

  *dest = 0x00;
  return res;
}
