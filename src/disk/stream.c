#include <stdbool.h>

#include "stream.h"
#include "memory/heap/kheap.h"
#include "config.h"

struct disk_stream* disk_stream_new(int disk_id) {
  struct disk* disk = disk_get(disk_id);
  if (!disk)
    return 0;

  struct disk_stream* stream = kzalloc(sizeof(struct disk_stream));
  stream->disk  = disk;
  stream->pos   = 0;

  return stream;
}

int disk_stream_seek(struct disk_stream* stream, int pos) {
  stream->pos = pos;
  return 0;
}

int disk_stream_read(struct disk_stream* stream, void* out, int total) {
  int sector        = stream->pos / ARUKAOS_SECTOR_SIZE,
      offset        = stream->pos % ARUKAOS_SECTOR_SIZE,
      total_to_read = total;

  bool overflow = (offset + total_to_read) >= ARUKAOS_SECTOR_SIZE;
  char buff[ARUKAOS_SECTOR_SIZE];

  // In case of overflow, total_to_read must be fit to make 
  // offset + total_to_read = 512 (ARUKAOS_SECTOR_SIZE)
  if (overflow)
    total_to_read -= (offset + total_to_read) - ARUKAOS_SECTOR_SIZE;

  int res = disk_read_block(stream->disk, sector, 1, buff);
  if (res < 0)
    goto out;

   for (int x = 0; x < total_to_read; x++) 
    *((char*)(out++)) = *(buff + offset + x);

  // Adjust the stream in case more sectors are required
  stream->pos += total_to_read;
  if (overflow)
    res = disk_stream_read(stream, out, total - total_to_read);

out:
  return res;
}

void disk_stream_close(struct disk_stream* stream) {
  kfree(stream);
}
