#include <stdint.h>

#include "disk.h"
#include "io/io.h"
#include "memory/memory.h"
#include "config.h"
#include "status.h"

struct disk disk;

int disk_read_sector(unsigned int lba, int total, void* buff) {
  
  outb(0x1F6, (lba >> 24) | 0xE0);
  outb(0x1F2, total);
  outb(0x1F3, (uint8_t)(lba & 0xFF));
  outb(0x1F4, (uint8_t)(lba >> 8));
  outb(0x1F5, (uint8_t)(lba >> 16));
  outb(0x1F7, 0x20);

  uint16_t* ptr = (uint16_t*)(buff);
  
  for (int b = 0; b < total; b++) {
    
    // Wait for buffer to be ready...
    
    char ch = insb(0x1F7);
    while (!(ch & 0x08))
      ch = insb(0x1F7);
    
    // Now, we're ready to read from the disk
    // Reading 512B at a time (512B = a sector in a disk)
    
    for (int i = 0; i < 256; i++) {
      *ptr = insw(0x1F0);
      ptr++;
    }
  }
  return 0;
}

void disk_search_and_init() {
  memset(&disk, 0x00, sizeof(disk));

  disk.type         = ARUKAOS_DISK_TYPE_REAL;
  disk.sector_size  = ARUKAOS_SECTOR_SIZE;
  disk.filesystem   = fs_resolve(&disk);
  disk.id           = 0;
}

struct disk* disk_get(int idx) {
  if (idx != 0)
    return 0;

  return &disk;
}

int disk_read_block(struct disk* idisk, unsigned int lba, int total, void* buff) {
  if (idisk != &disk)
    return -EIO;
  
  return disk_read_sector(lba, total, buff);
}
