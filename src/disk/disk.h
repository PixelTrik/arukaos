#ifndef DISK_H
#define DISK_H

#define ARUKAOS_DISK_TYPE_REAL  0

#include "fs/file.h"

struct disk {
  unsigned int type;
  int sector_size;
  int id;
  struct filesystem* filesystem;
  
  // This is a generic pointer since different filesystems have different
  // structures.
  void* fs_private;
};

void disk_search_and_init();
struct disk* disk_get(int idx);
int disk_read_block(struct disk* disk, unsigned int lba, int total, void* buff);

#endif
