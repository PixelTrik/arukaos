#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <stdbool.h>

typedef int (*KEYBOARD_INIT_FUNCTION)();
struct process;

struct keyboard {
  KEYBOARD_INIT_FUNCTION init;
  char name[20];
  struct keyboard* next;
  
  bool is_caps;
};

void keyboard_init();
void keyboard_backspace(struct process* process);
void keyboard_push(char ch);
char keyboard_pop();
int keyboard_insert(struct keyboard* keyboard);

#endif
