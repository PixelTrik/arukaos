#include <stdint.h>
#include <stddef.h>

#include "kernel.h"
#include "idt/idt.h"
#include "classic.h"
#include "keyboard.h"
#include "io/io.h"
#include "task/task.h"

#define CLASSIC_KEYBOARD_CAPS_LOCK  0x3A

int classic_keyboard_init();

// Hex Legends
// 0x1B = Escape Key
// 0x08 = Backspace Key
// 0x0D = Enter Key
// 0x20 = Space Key

static uint8_t keyboard_scan_set_one[] = {
  0x00, 0x1B, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 0x08,
  '\t', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', 0x0d, 0x00,
  'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', '`',  0x00, '\\',
  'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/', 0x00, '*', 0x00, 0x20,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  // For the numpad
  '7', '8', '9', '-', '4', '5', '6', '+', '1', '2', '3', '0', '.'
};

struct keyboard classic_keyboard = {
  .name = {"Classic"},  // This is classic PS/2 based keyboard
  .init = classic_keyboard_init
};

void classic_keyboard_handle_interrupt();

int classic_keyboard_init() {
  idt_register_interrupt_callback(ISR_KEYBOARD_INTERRUPT, classic_keyboard_handle_interrupt);
  classic_keyboard.is_caps = false;
  outb(PS2_PORT1, PS2_PORT1_ENABLED);
  return 0;
}

uint8_t classic_keyboard_scancode_to_char(uint8_t scancode) {
  size_t keyboard1_size = sizeof(keyboard_scan_set_one) / sizeof(uint8_t);
  if (scancode > keyboard1_size)
    return 0;

  char key_ch = keyboard_scan_set_one[scancode];
  if (!classic_keyboard.is_caps)
    key_ch = (key_ch >= 'A' && key_ch <= 'Z') ? key_ch + 32 : key_ch;

  return key_ch;
}

void classic_keyboard_handle_interrupt() {
  kernel_page();
  uint8_t scancode = 0;
  scancode = insb(KEYBOARD_INPUT_PORT);
  
  // Ignoring rogue IRQ (Interrupt ReQuest)
  insb(KEYBOARD_INPUT_PORT);

  if (scancode & CLASSIC_KEY_RELEASED)
    return;

  if (scancode == CLASSIC_KEYBOARD_CAPS_LOCK)
    classic_keyboard.is_caps = !classic_keyboard.is_caps;

  keyboard_push(classic_keyboard_scancode_to_char(scancode));

  task_page();
}

struct keyboard* classic_init() {
  return &classic_keyboard;
}
