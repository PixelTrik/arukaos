#include "keyboard.h"
#include "status.h"
#include "kernel.h"
#include "task/process.h"
#include "task/task.h"
#include "classic.h"

static struct keyboard* keyboard_list_head = 0;
static struct keyboard* keyboard_list_tail = 0;

void keyboard_init() {
  keyboard_insert(classic_init());
}

int keyboard_insert(struct keyboard* keyboard) {
  if (!keyboard->init)
    return -EINVARG;

  if (keyboard_list_tail) {
    keyboard_list_tail->next = keyboard;
    keyboard_list_tail = keyboard;
  }
  else
    keyboard_list_tail = keyboard_list_head = keyboard;

  return keyboard->init();
}

static int keyboard_get_tail_idx(struct process process) {
  return process.keyboard.tail % sizeof(process.keyboard.buff);
}

void keyboard_backspace(struct process* process) {
  process->keyboard.tail -= 1;
  int idx = keyboard_get_tail_idx(*process);
  process->keyboard.buff[idx] = 0x00;
}

void keyboard_push(char ch) {
  struct process* process = process_current();
  if (!process || ch == 0x00)
    return;

  int idx = keyboard_get_tail_idx(*process);
  process->keyboard.buff[idx] = ch;
  process->keyboard.tail++; 
}

char keyboard_pop() {
  if (!task_current())
    return 0;

  struct process* process = task_current()->process;
  int idx = process->keyboard.head % sizeof(process->keyboard.buff);
  char ch = process->keyboard.buff[idx];
  if (ch == 0x00)
    return 0;

  process->keyboard.buff[idx] = 0x00;
  process->keyboard.head++;
  return ch;
}
