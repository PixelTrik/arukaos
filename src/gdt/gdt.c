#include "gdt.h"
#include "kernel.h"

void encode_gdt_entry(uint8_t* dst, struct gdt_structured src) {
  if ((src.limit > 65536) && ((src.limit & 0xFFF) != 0xFFF))
    panic("enocde_gdt_entry: Invalid Argument");

  dst[6] = 0x40;
  if (src.limit > 65536) {
    src.limit >>= 12;
    dst[6] = 0xC0;
  }

  // Encoding limit
  dst[0] = src.limit & 0xFF;
  dst[1] = (src.limit >> 8) & 0xFF;
  dst[6] |= (src.limit >> 16) & 0x0F;

  // Encoding base
  dst[2] = src.base & 0xFF;
  dst[3] = (src.base >> 8) & 0xFF;
  dst[4] = (src.base >> 16) & 0xFF;
  dst[7] = (src.base >> 24) & 0xFF;

  // Encoding Type
  dst[5] = src.type;
}

void gdt_structured_to_gdt(struct gdt* gdt, struct gdt_structured *structured_gdt, int entries) {
  for (int x = 0; x < entries; x++)
    encode_gdt_entry((uint8_t*)(gdt + x), *(structured_gdt + x));
}
