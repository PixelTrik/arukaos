section .asm
global gdt_load

gdt_load:
  mov   eax, [esp + 4]
  mov   [gdt_desc + 2], eax
  mov   ax, [esp + 8]
  mov   [gdt_desc], ax
  lgdt  [gdt_desc]
  ret

section .data
gdt_desc:
  dw 0x00 ; Size
  dw 0x00 ; GDT Start Address
