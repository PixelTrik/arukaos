[BITS 32]

global _start
global kernel_registers
extern kernel_main

CODE_SEG equ 0x08
DATA_SEG equ 0x10

_start:
  mov ax, DATA_SEG
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  mov ebp, 0x00200000 
  mov esp, ebp

  ; Enable A20 line to get 21st bit in memory access
  in al, 0x92
  or al, 2
  out 0x92, al

  ; Remapping Master PIC
  mov al, 00010001b
  out 0x20, al
  
  ; Start Master ISR at interrupt 0x20
  mov al, 0x20
  out 0x21, al

  mov al, 0x01
  out 0x21, al

  ; Master PIC remapped

  call kernel_main
  jmp $

kernel_registers:
  mov ax, 0x10
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  ret

times 512 - ($ - $$) db 0
