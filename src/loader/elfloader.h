#ifndef ELFLOADER_H
#define ELFLOADER_H

#include <stdint.h>
#include <stddef.h>

#include "elf.h"
#include "config.h"

struct elf_file {
  char  filename[ARUKAOS_MAX_PATH];
  int   in_mem_size;
  void* elf_mem;
  void* virt_base_addr; 
  void* virt_end_addr;
  void* phys_base_addr;
  void* phys_end_addr;
};

int elf_load(const char* filename, struct elf_file** file_out);
void elf_close(struct elf_file* file);

void* elf_virt_base(struct elf_file* file);
void* elf_virt_end(struct elf_file* file);
void* elf_phys_base(struct elf_file* file);
void* elf_phys_end(struct elf_file* file);

struct elf_header* elf_header(struct elf_file* file);
struct elf32_shdr* elf_sheader(struct elf_header* header);
struct elf32_phdr* elf_pheader(struct elf_header* header);
void* elf_memory(struct elf_file* file);
struct elf32_phdr* elf_program_header(struct elf_header* header, int index);
struct elf32_shdr* elf_section(struct elf_header* header, int index);
void* elf_phdr_phys_addr(struct elf_file* file, struct elf32_phdr* phdr);

#endif
