#include <stdbool.h>

#include "elf.h"
#include "elfloader.h"
#include "status.h"
#include "kernel.h"
#include "config.h"
#include "fs/file.h"
#include "memory/memory.h"
#include "memory/heap/kheap.h"
#include "memory/paging/paging.h"
#include "string/string.h"

const char elf_signature[] = {0x7f, 'E', 'L', 'F'};

static bool elf_valid_signature(void* buffer) {
  return memcmp(buffer, (void*)(elf_signature), sizeof(elf_signature)) == 0;
}

static bool elf_valid_class(struct elf_header* header) {
  return header->e_ident[EI_CLASS] == ELFCLASSNONE || header->e_ident[EI_CLASS] == ELFCLASS32;
}

static bool elf_valid_encoding(struct elf_header* header) {
  return header->e_ident[EI_DATA] == ELFDATANONE || header->e_ident[EI_DATA] == ELFDATA2LSB;
}

static bool elf_is_executable(struct elf_header* header) {
  return header->e_type == ET_EXEC && header->e_entry >= ARUKAOS_PROGRAM_VIRTUAL_ADDR;
}

static bool elf_has_program_header(struct elf_header* header) {
  return header->e_phoff != 0;
}

void* elf_memory(struct elf_file* file) {
  return file->elf_mem;
}

struct elf_header* elf_header(struct elf_file* file) {
  return file->elf_mem;
}

struct elf32_shdr* elf_sheader(struct elf_header* header) {
  return (struct elf32_shdr*)((int)(header) + header->e_shoff);
}

struct elf32_phdr* elf_pheader(struct elf_header* header) {
  return (struct elf32_phdr*)((header->e_phoff == 0) ? 0 : (int)(header) + header->e_phoff);
}

struct elf32_phdr* elf_program_header(struct elf_header* header, int index) {
  return elf_pheader(header) + index;
}

struct elf32_shdr* elf_section(struct elf_header* header, int index) {
  return elf_sheader(header) + index;
}

void* elf_phdr_phys_addr(struct elf_file* file, struct elf32_phdr* phdr) {
  return elf_memory(file) + phdr->p_offset;
}

char* elf_str_table(struct elf_header* header) {
  return (char*)(header) + elf_section(header, header->e_shstrndx)->sh_offset;
}

void* elf_virt_base(struct elf_file* file) {
  return file->virt_base_addr;
}

void* elf_virt_end(struct elf_file* file) {
  return file->virt_end_addr;
}

void* elf_phys_base(struct elf_file* file) {
  return file->phys_base_addr;
}

void* elf_phys_end(struct elf_file* file) {
  return file->phys_end_addr;
}

int elf_validate_loaded(struct elf_header* header) {
  return (elf_valid_signature(header) && elf_valid_class(header) && elf_valid_encoding(header) && elf_has_program_header(header)) ? ARUKAOS_ALL_OK : -EINFORMAT;
}

int elf_process_phdr_pt_load(struct elf_file* elf_file, struct elf32_phdr* phdr) {
  /*
   * By selecting the lowest base address and highest end address, we can allocate
   * the maximum amount of memory for loaded ELF file.
  */
  if (elf_file->virt_base_addr >= (void*)(phdr->p_vaddr) || elf_file->virt_base_addr == 0x00) {
    elf_file->virt_base_addr = (void*)(phdr->p_vaddr);
    elf_file->phys_base_addr = elf_memory(elf_file) + phdr->p_offset;
  }

  unsigned int end_vaddr = phdr->p_vaddr + phdr->p_filesz;
  if (elf_file->virt_end_addr <= (void*)(end_vaddr) || elf_file->virt_end_addr == 0x00) {
    elf_file->virt_end_addr = (void*)(end_vaddr);
    elf_file->phys_end_addr = elf_memory(elf_file) + phdr->p_offset + phdr->p_filesz;
  }
  return 0;
}

int elf_process_pheader(struct elf_file* elf_file, struct elf32_phdr* phdr) {
  int res = 0;
  switch(phdr->p_type) {
    case PT_LOAD: res = elf_process_phdr_pt_load(elf_file, phdr); break;
  }
  return res;
}

int elf_process_pheaders(struct elf_file* elf_file) {
  int res = 0;
  struct elf_header* header = elf_header(elf_file);
  for (int x = 0; x < header->e_phnum; x++) {
    struct elf32_phdr* phdr = elf_program_header(header, x);
    res = elf_process_pheader(elf_file, phdr);
    if (res < 0)
      break;
  }
  return res;
}

int elf_process_loaded(struct elf_file* file) {
  int res = 0;
  struct elf_header* header = elf_header(file);
  res = elf_validate_loaded(header);
  if (res < 0)
    goto out;

  res = elf_process_pheaders(file);
  if (res < 0)
    goto out;

out:
  return res;
}

int elf_load(const char* filename, struct elf_file** file_out) {
  struct elf_file* elf_file = kzalloc(sizeof(struct elf_file));
  int fd = 0;
  int res = fopen(filename, "r");
  if (res <= 0) {
    res = -EIO;
    goto out;
  }

  fd = res;
  struct file_stat stat;
  res = fstat(fd, &stat);
  if (res < 0)
    goto out;

  elf_file->elf_mem = kzalloc(stat.filesize);
  res = fread(elf_file->elf_mem, stat.filesize, 1, fd);
  if (res < 0)
    goto out;

  res = elf_process_loaded(elf_file);
  if (res < 0)
    goto out;

  *file_out = elf_file;

out:
  fclose(fd);
  return res;
}

void elf_close(struct elf_file* file) {
  if (!file)
    return;

  kfree(file->elf_mem);
  kfree(file);
}
